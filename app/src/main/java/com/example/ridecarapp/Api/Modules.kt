package com.cmexpertise.mealkey.Api


import com.cmexpertise.mealkey.Repository.YourDetailRepository
import com.cmexpertise.mealkey.Repository.YourDetailRepositoryImpl
import com.cmexpertise.mealkey.Utils.CAT_API_BASE_URL
import com.cmexpertise.mealkey.mvvm.profile.YourDetailViewModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.Repository.*
import com.example.ridecarapp.mvvm.login_new.CheckOtpViewModel
import com.example.ridecarapp.mvvm.login_new.LoginViewModel
import com.example.ridecarapp.mvvm.login_new.VehicleBrandViewModel
import com.example.ridecarapp.mvvm.login_new.VehicleModelViewModel
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Array.get
import java.util.concurrent.TimeUnit
import org.koin.androidx.viewmodel.dsl.viewModel

sealed class UsesCaseResult<out T: Any> {
    class Success<out T: Any>(val data: T) : UsesCaseResult<T>()
    class Failed(val exception: Throwable) : UsesCaseResult<Nothing>()
}

val appmodules = module {
    single {
        WebSerivice<Apis>(
            okHttpClient = createOkhttpclient(),
            factory = RxJava2CallAdapterFactory.create(),
            baseurl = CAT_API_BASE_URL
        )
    }
    single { ApplicationClass.mInstance!!.applicationContext }

    factory<YourDetailRepository> { YourDetailRepositoryImpl(get()) }
    viewModel { YourDetailViewModel(get()) }

    factory<LoginRepository> { LoginNewRepositoryImpl(get()) }
    viewModel { LoginViewModel(get()) }

    factory<CheckOtpRepository> { CheckOtpRepositoryImpl(get()) }
    viewModel { CheckOtpViewModel(get()) }

    factory<BrandListRepository> { BrandListRepositoryImpl(get()) }
    viewModel { VehicleBrandViewModel(get()) }

    factory<VehicleModelListRepository> { VehicleModelListRepositoryImpl(get()) }
    viewModel { VehicleModelViewModel(get()) }
}

fun createOkhttpclient(): OkHttpClient {
    val interceptor = HttpLoggingInterceptor()
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
    val okHttpClient = OkHttpClient.Builder()
    okHttpClient.addInterceptor(interceptor)
    okHttpClient.connectTimeout(5, TimeUnit.MINUTES)
    okHttpClient.readTimeout(5, TimeUnit.MINUTES)
    okHttpClient.writeTimeout(100, TimeUnit.SECONDS)
    okHttpClient.retryOnConnectionFailure(true)
    okHttpClient.protocols(listOf(Protocol.HTTP_1_1))

    return okHttpClient.build()
}

inline fun <reified T> WebSerivice(okHttpClient: OkHttpClient, factory: CallAdapter.Factory, baseurl: String): T{
    var retrofit = Retrofit.Builder()
        .baseUrl(baseurl)
        .addCallAdapterFactory(factory)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(okHttpClient)
        .build()
    return retrofit.create(T::class.java)
}