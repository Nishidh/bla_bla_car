package com.cmexpertise.mealkey.Api


import com.cmexpertise.mealkey.Utils.*
import com.example.ridecarapp.model.login.LoginResponse
import com.example.ridecarapp.model.otp.CheckOtpResponse
import com.example.ridecarapp.model.vehiclebrand.BrandResponse
import com.example.ridecarapp.model.vehiclemodel.VehicleModelResposne
import com.example.ridecarapp.model.your_detail.YourDetailResponse
import kotlinx.coroutines.Deferred
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface Apis {



    @Multipart
    @POST(Constans_YourDetail)
    fun doUpdateUserDetailWithImage(
        @Part("user_id") user_id: RequestBody?,
        @Part("first_name") first_name: RequestBody?,
        @Part("last_name") last_name: RequestBody?,
        @Part("gender") gender: RequestBody?,
        @Part("birth_year") birth_year: RequestBody?,
        @Part("mobile") mobile: RequestBody?,
        @Part("country_code") country_code: RequestBody?,
        @Part("email") email: RequestBody?,
        @Part("mini_bio") mini_bio: RequestBody?,
        @Part("device_id") device_id: RequestBody?,
        @Part("device_token") device_token: RequestBody?,
        @Part("device_type") device_type: RequestBody?,
        @Part image: MultipartBody.Part?
    ): Deferred<YourDetailResponse>

    @Multipart
    @POST(Constans_YourDetail)
    fun doUpdateUserDetailWithoutImage(
        @Part("user_id") user_id: RequestBody?,
        @Part("first_name") first_name: RequestBody?,
        @Part("last_name") last_name: RequestBody?,
        @Part("gender") gender: RequestBody?,
        @Part("birth_year") birth_year: RequestBody?,
        @Part("mobile") mobile: RequestBody?,
        @Part("country_code") country_code: RequestBody?,
        @Part("email") email: RequestBody?,
        @Part("mini_bio") mini_bio: RequestBody?,
        @Part("device_id") device_id: RequestBody?,
        @Part("device_token") device_token: RequestBody?,
        @Part("device_type") device_type: RequestBody?
    ): Deferred<YourDetailResponse>


    @FormUrlEncoded
    @POST(Constans_login)
    fun doLogin(
        @Field("mobile") email: String,
        @Field("country_code") password: String
    ): Deferred<LoginResponse>

    @FormUrlEncoded
    @POST(Constans_check_otp)
    fun doCheckOtp(
        @Field("mobile") email: String,
        @Field("country_code") password: String,
        @Field("otp") otp: String
    ): Deferred<CheckOtpResponse>

    @FormUrlEncoded
    @POST(Constans_getVehicle_models)
    fun getModelList(
        @Field("vehicle_brand_id") vehicle_brand_id: String
    ): Deferred<VehicleModelResposne>


    @GET(Constans_getVehicle_brand)
    fun getBrandList(): Deferred<BrandResponse>

}