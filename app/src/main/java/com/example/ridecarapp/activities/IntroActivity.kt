package com.example.ridecarapp.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.cmexpertise.mealkey.activities.BaseActivity
import com.cmexpertise.mealkey.adapater.IntroScreenAdapter
import com.cmexpertise.mealkey.models.IntroModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.Utils.PREF_IS_FIRSTTIME
import com.example.ridecarapp.databinding.ActivityIntroBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.IndicatorAdapter

class IntroActivity : BaseActivity() {

    private lateinit var binding: ActivityIntroBinding

    //Viewpager adapter
    private var introList: ArrayList<IntroModel> = ArrayList()
    private lateinit var introScreenAdapter: IntroScreenAdapter
    private lateinit var indicatorAdapter: IndicatorAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_intro
        )
        setListData()
        setViewPagerAdapter()
        setUpIndicatorAdapter()
        setViewpagerScrollListinter()
        setOnClickLisnter()
    }

    override fun onSingleClick(rootView: View) {
        when(rootView.id){

            R.id.tvSkip -> {
                //ApplicationClass.mInstance?.savePreferenceDataBoolean(resources.getString(R.string.PREF_IS_FIRSTTIME), true)
                val i = Intent(this@IntroActivity, LoginActivity::class.java)
                startActivity(i)
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out)
                finish()
                ApplicationClass.mInstance?.savePreferenceDataBoolean(PREF_IS_FIRSTTIME,true)
            }
        }
    }

    private fun setOnClickLisnter() {
        binding.tvSkip.setOnClickListener(this)
    }


    @SuppressLint("NewApi")
    private fun setViewpagerScrollListinter() {
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }


            override fun onPageSelected(position: Int) {
                if(position==2)
                    binding.tvSkip.text=resources.getString(R.string.Done)
                else
                    binding.tvSkip.text=resources.getString(R.string.skip)

                indicatorAdapter.updateLastPosition(position)
            }
        })
    }

    private fun setUpIndicatorAdapter() {
        binding.rvIndicatorList.layoutManager = LinearLayoutManager(this,
            LinearLayoutManager.HORIZONTAL,false)
        indicatorAdapter = IndicatorAdapter(this, introList)
        binding.rvIndicatorList.adapter = indicatorAdapter
    }

    private fun setViewPagerAdapter() {
        introScreenAdapter = IntroScreenAdapter(this, introList)
        binding.viewPager.adapter = introScreenAdapter
       /* binding.viewPager.setPageTransformer(true, CubeInPageTransformer())*/
    }

    private fun setListData() {
        introList.add(IntroModel(R.drawable.intro_1, "Check Your Ride"))
        introList.add(IntroModel(R.drawable.intro_2, "Easy Booked"))
        introList.add(IntroModel(R.drawable.intro_3, "Enjoy The Ride"))

    }
}
