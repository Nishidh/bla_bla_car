package com.example.ridecarapp.activities

import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.style.ImageSpan
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.ridecarapp.R


class TestActivity : AppCompatActivity() {

    private var textview:TextView ?= null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        textview = findViewById(R.id.textview)

        val myIcon: Drawable = getResources().getDrawable(R.drawable.ic_rightarrow)
        textview?.getLineHeight()?.let { myIcon.setBounds(0, 0, it, textview?.getLineHeight()!!) };
        val builder = SpannableStringBuilder()
        builder.append("Express Highway")
            .append(" ", ImageSpan(myIcon), 0)
            .append("Toll Plaza - Ahemdabad Vadodara Express Way [NE-1]")

        textview?.text = builder

    }
}
