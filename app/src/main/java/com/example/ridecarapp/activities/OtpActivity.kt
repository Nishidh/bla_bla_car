package com.example.ridecarapp.activities

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.cmexpertise.mealkey.Utils.Constans_country_code
import com.cmexpertise.mealkey.Utils.Constans_mobile_no
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.activities.BaseActivity
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.Utils.PREF_IS_FIRSTTIME
import com.example.ridecarapp.Utils.PREF_LOGIN
import com.example.ridecarapp.databinding.ActivityOtpBinding
import com.example.ridecarapp.model.otp.Data
import com.example.ridecarapp.mvvm.login_new.CheckOtpViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class OtpActivity : BaseActivity() {

    private lateinit var binding: ActivityOtpBinding
    private val checkOtpViewModel : CheckOtpViewModel by viewModel()

    private var mobileNo:String?=null
    private var countryCode:String?=null
    private var isResend=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_otp)
        binding.model=checkOtpViewModel
        initComponents()

    }


    private fun initComponents() {
        intent.apply {
            mobileNo=getStringExtra(Constans_mobile_no)
            countryCode=getStringExtra(Constans_country_code)
        }

        setOnClickListener()
        setupObserver()
        startTrimer()
    }


    private fun setupObserver() {
        checkOtpViewModel.responseData.observe(this, Observer { data ->
            binding.progressBar.visibility=View.GONE
            if(data.status==1){
                setData(data.data)
                val mMainActivity = Intent(this@OtpActivity, MainActivity::class.java)
                mMainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(mMainActivity)
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out)
                finish()
            }else{
                ObjectAnimator.ofFloat(binding!!.otpView, "translationX", 0f, 25f, -25f, 25f, -25f, 15f, -15f, 6f, -6f, 0f)
                    .setDuration(500)
                    .start()
                Utils.showToast(this,data.message)
            }
        })


        checkOtpViewModel.showerror.observe(this, Observer {

        })

        checkOtpViewModel.loginData.observe(this, Observer {

            binding.progressBar.visibility=View.GONE
            if(it.status==1){
                Toast.makeText(this,""+it.otp,Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun setData(data: Data) {
        data?.apply {
            ApplicationClass.mInstance?.savePreferenceDataBoolean(PREF_LOGIN,true)
        }
    }


    fun startTrimer(){
        object : CountDownTimer(30 * 2000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                isResend=false
                var seconds: Int = (millisUntilFinished / 1000).toInt()
                val hours: Int = seconds / (60 * 60)
                val tempMint: Int = seconds - hours * 60 * 60
                val minutes = tempMint / 60
                seconds = tempMint - minutes * 60
                binding.tvTimer.text=String.format("%02d",minutes)+" : "+String.format("%02d",seconds)
            }
            override fun onFinish() {
                isResend=true
                binding.tvTimer.text=resources.getString(R.string.str_resend)
            }
        }.start()
    }



    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.rlContinueBtn ->{

                if(binding.otpView.otp.toString().isNotEmpty()){
                    callApi()
                }else{
                    Utils.showToast(this,resources.getString(R.string.Pls_enter_otp))
                }

            }
            R.id.llChangeNumber->{finish()}
            R.id.tvTimer->{
                if(isResend){
                    startTrimer()
                    callGetOtpApi()
                }
            }
        }
    }

    private fun callGetOtpApi() {
        if (Utils.isOnline(this, true)) {
            binding.progressBar.visibility = View.VISIBLE
            checkOtpViewModel.doGetOtp(mobileNo,countryCode)
        }
    }


    private fun setOnClickListener() {
        binding.rlContinueBtn.setOnClickListener(this)
        binding.llChangeNumber.setOnClickListener(this)
        binding.tvTimer.setOnClickListener(this)
    }

    private fun callApi() {
        if (Utils.isOnline(this, true)) {
            binding.progressBar.visibility = View.VISIBLE
            checkOtpViewModel.checkOtp(mobileNo,countryCode,binding.otpView.otp)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out)
    }
}
