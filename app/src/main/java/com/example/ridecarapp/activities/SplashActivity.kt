package com.example.ridecarapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.Utils.PREF_IS_FIRSTTIME
import com.example.ridecarapp.Utils.PREF_LOGIN

class SplashActivity : AppCompatActivity() {

    private var mHandler: Handler? = null
    private val delay: Long = 1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_splash)


        //window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        //Utils.setLightStatusBar(window.decorView)

        mHandler = Handler()
        mHandler!!.postDelayed(runnable, delay)

    }

    val runnable: Runnable = Runnable {

       /* val mMainActivity = Intent(this@SplashActivity, IntroActivity::class.java)
        startActivity(mMainActivity)
        overridePendingTransition(
            R.anim.anim_right_in,
            R.anim.anim_left_out
        )
        finish()*/



        val isLogin = ApplicationClass.mInstance?.sharedPreferences
            ?.getBoolean(PREF_LOGIN, false)


        val isFirstTime = ApplicationClass.mInstance?.sharedPreferences
            ?.getBoolean(PREF_IS_FIRSTTIME, false)


        if(!isFirstTime!!){
            val mMainActivity = Intent(this@SplashActivity, IntroActivity::class.java)
            startActivity(mMainActivity)
            overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out)
            finish()
        }else{

            if (isLogin!!) {
                val mMainActivity = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(mMainActivity)
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out)
                finish()
            }else{
                val mMenuIntent = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(mMenuIntent)
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out)
                finish()

            }
        }



        /*if (!isFinishing) {


            val isLogin = ApplicationClass.mInstance?.sharedPreferences
                ?.getBoolean(getString(R.string.PREF_LOGIN), false)

            val isSignUp = ApplicationClass.mInstance?.sharedPreferences
                ?.getBoolean(getString(R.string.PREF_SIGNUP), false)


            val isFirstTime = ApplicationClass.mInstance?.sharedPreferences
                ?.getBoolean(getString(R.string.PREF_IS_FIRSTTIME), false)

            val isSelectLoaction = ApplicationClass.mInstance?.sharedPreferences
                ?.getBoolean(getString(R.string.PREF_IS_LOCATIONSELECT), false)


            if(!isFirstTime!!){
                val mMainActivity = Intent(this@SplashActivity, IntroActivity::class.java)
                startActivity(mMainActivity)
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out)
                finish()
            }else{
                if (isLogin!!) {
                    if(!isSelectLoaction!!) {

                        val mMenuIntent = Intent(this@SplashActivity, SelectLocationActivity::class.java)
                        mMenuIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        mMenuIntent.putExtra("type","home")
                        startActivity(mMenuIntent)
                        overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out)

                    }else{
                        val mMainActivity = Intent(this@SplashActivity, MainActivity::class.java)
                        startActivity(mMainActivity)
                        overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out)
                        finish()
                    }

                } else {
                    val mMenuIntent = Intent(this@SplashActivity, LoginActivityNew::class.java)
                    startActivity(mMenuIntent)
                    overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out)
                    finish()

                }

            }


        }*/
    }

}
