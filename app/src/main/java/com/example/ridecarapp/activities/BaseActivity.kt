package com.cmexpertise.mealkey.activities

import android.os.Bundle
import android.os.SystemClock
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.ridecarapp.R

abstract class BaseActivity : AppCompatActivity() ,View.OnClickListener {

    private var mLastClickTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
    }

    abstract fun onSingleClick(rootView: View)

    override fun onClick(v: View?) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < MAX_CLICK_INTERVAL) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        v?.let { onSingleClick(it) }
    }

    companion object {
        private val MAX_CLICK_INTERVAL = 700
    }

}