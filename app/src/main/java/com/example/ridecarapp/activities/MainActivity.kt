package com.example.ridecarapp.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.ActivityMainBinding
import com.example.ridecarapp.fragments.*

class  MainActivity : AppCompatActivity(),View.OnClickListener {

    private lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        ApplicationClass.mInstance.setActity(this@MainActivity)

        setOnClickLisnter()

        var homeFragment = HomeFragment()

        val manager: FragmentManager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.add(R.id.container, homeFragment)
        transaction.commit()
    }

    private fun setOnClickLisnter() {
        binding.ivBack.setOnClickListener(this)
        binding.ivFilter.setOnClickListener(this)
    }

    fun setUpToolBar(title:String,isShowBack:Boolean, isShowToolBar:Boolean){
        binding.ivFilter.visibility=View.GONE
        binding.tvTitle.text=title
        if(isShowBack) binding.ivBack.visibility= View.VISIBLE else binding.ivBack.visibility=View.GONE
        if(isShowToolBar) binding.rlToolbar.visibility= View.VISIBLE else binding.rlToolbar.visibility=View.GONE
    }
    fun setUpToolBarFiltter(title:String,isShowBack:Boolean, isShowToolBar:Boolean){
        binding.ivFilter.visibility=View.VISIBLE
        binding.tvTitle.text=title
        if(isShowBack) binding.ivBack.visibility= View.VISIBLE else binding.ivBack.visibility=View.GONE
        if(isShowToolBar) binding.rlToolbar.visibility= View.VISIBLE else binding.rlToolbar.visibility=View.GONE
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.ivBack-> supportFragmentManager.popBackStack()
            R.id.ivFilter->{
                Utils.addNextFragment(this,FilterFragment(),Utils.currentFragment,true)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(Utils.currentFragment is FindRideMap){
            var fragment : FindRideMap= Utils.currentFragment as FindRideMap
            fragment.onActivityResult(requestCode,resultCode,data)
        }
    }
}
