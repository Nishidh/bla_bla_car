package com.example.ridecarapp.activities

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.cmexpertise.mealkey.Utils.Constans_country_code
import com.cmexpertise.mealkey.Utils.Constans_mobile_no
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.activities.BaseActivity
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.Utils.PREF_LOGIN
import com.example.ridecarapp.databinding.ActivityLoginBinding
import com.example.ridecarapp.mvvm.login_new.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginActivity : BaseActivity() {

    private var binding : ActivityLoginBinding ?= null
    private val loginViewModel : LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login)
        binding?.viewModel = loginViewModel

        initComponents()

    }


    private fun initComponents() {
        setOnClickListener()
        setupObserver()
    }

    private fun setupObserver() {
        loginViewModel.loginData.observe(this, Observer { data ->

            if(data.status==1){
                Toast.makeText(this,""+data.otp,Toast.LENGTH_LONG).show()
                val intent = Intent(this@LoginActivity, OtpActivity::class.java)
                intent.apply {
                    putExtra(Constans_mobile_no,binding!!.etMobile.text.toString())
                    putExtra(Constans_country_code,binding!!.etDialCode.selectedCountryCode)
                }
                startActivity(intent)
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out)
            }else{
                Utils.showToast(this,data.message)
            }
        })


        loginViewModel.showerror.observe(this, Observer {

        })

    }

    private fun setOnClickListener() {
        binding?.btnNext?.setOnClickListener(this)
    }


    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.btnNext ->{
                if(loginViewModel.isValid(binding!!.etMobile.text.toString()).equals("")){
                    callApi()
                }else{
                    Utils.showToast(this,loginViewModel.isValid(binding!!.etMobile.text.toString()))
                }

            }
        }
    }

    private fun callApi() {
        if (Utils.isOnline(this, true)) {
            binding?.progressBar?.visibility = View.VISIBLE
            loginViewModel.doGetOtp(binding?.etMobile?.text.toString(),binding?.etDialCode?.selectedCountryCode)
        }
    }



}
