package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentVehiclePhotoBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.VehiclePhotoAdapter

class VehiclePhotoFragment : BaseFragment() {

    private lateinit var binding: FragmentVehiclePhotoBinding
    private lateinit var activity: AppCompatActivity

    private lateinit var adapter: VehiclePhotoAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_vehicle_photo, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        setUpToolBar()
        return view
    }


    override fun initComponents(view: View) {
        setUpList()
        setUpAdapter()
        setOnClickListenr()
    }

    private fun setOnClickListenr() {
        binding.rlNext.setOnClickListener(this)
    }

    private fun setUpList() {
        for(i in 1..5){
            arrayList.add(TestModel(""))
        }
    }

    private fun setUpAdapter() {
        binding.rvVehiclePhoto.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL ,false)
        binding.rvVehiclePhoto.isNestedScrollingEnabled = false
        adapter = VehiclePhotoAdapter(activity, arrayList)
        binding.rvVehiclePhoto.adapter = adapter
    }

    override fun onSingleClick(rootView: View) {

        when(rootView.id){
            R.id.rlNext->{
                Utils.addNextFragment(activity,CardDetailsYearFragment(),this,false)
            }
        }
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Vehicle Photos",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

}
