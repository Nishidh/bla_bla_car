package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentVehicleTypeBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.VehicleTypeAdapter

class VehicleTypeFragment : BaseFragment() {

    private lateinit var binding: FragmentVehicleTypeBinding
    private lateinit var activity: AppCompatActivity

    private lateinit var adapter: VehicleTypeAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_vehicle_type, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        setUpToolBar()
        return view
    }


    override fun initComponents(rootView: View) {
        setUpList()
        setUpAdapter()
    }

    override fun onSingleClick(rootView: View) {
    }

    private fun setUpList() {
        for(i in 1..10){
            arrayList.add(TestModel(""))
        }
    }

    private fun setUpAdapter() {
        binding.rvVehicleType.layoutManager = LinearLayoutManager(activity)
        binding.rvVehicleType.isNestedScrollingEnabled = false
        adapter = VehicleTypeAdapter(this,activity, arrayList)
        binding.rvVehicleType.adapter = adapter
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Vehicle Type",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

    fun setOnItemClick() {
        Utils.addNextFragment(activity,VehicleColorFragment(),this,false)
    }
}
