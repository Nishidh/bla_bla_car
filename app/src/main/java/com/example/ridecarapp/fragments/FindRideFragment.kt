package com.example.ridecarapp.fragments

import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentFindRideBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.SearchRideAdapter
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class FindRideFragment : BaseFragment()  {

    private lateinit var binding: FragmentFindRideBinding
    private lateinit var activity: AppCompatActivity

    private lateinit var adapter: SearchRideAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()

    var day = 0
    var month: Int = 0
    var year: Int = 0
    var hour: Int = 0
    var minute: Int = 0
    var myDay = 0
    var myMonth: Int = 0
    var myYear: Int = 0
    var myHour: Int = 0
    var myMinute: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_find_ride, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        return view
    }


    override fun initComponents(view: View) {
        setUpToolBar()
        setOnClickListener()
        setUpList()
        setUpAdapter()
    }

    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.btnSearch -> {
                Utils.addNextFragment(activity,CreateRideAlterFragment(),this,false)
            }

            R.id.llPassenger -> {
                showDialog()
            }

            R.id.llDateTimePicker -> {
                pickDateTime()
            }

            R.id.cardYourLocation -> {
                val fragment = FindRideMap()
                val bundle = Bundle()
                bundle.putInt("type",1)
                fragment.arguments = bundle
                fragment.setTargetFragment(this,100)
                Utils.addNextFragment(activity,fragment,this,false)
            }


            R.id.cardWhereDoYou -> {
                val fragment = FindRideMap()
                val bundle = Bundle()
                bundle.putInt("type",1)
                fragment.arguments = bundle
                fragment.setTargetFragment(this,300)
                Utils.addNextFragment(activity,fragment,this,false)
            }

        }
    }

    private fun openFindRideMapFragment(){

    }


    private fun setOnClickListener() {
        binding.btnSearch.setOnClickListener(this)
        binding.llPassenger.setOnClickListener(this)
        binding.llDateTimePicker.setOnClickListener(this)
        binding.cardYourLocation.setOnClickListener(this)
        binding.cardWhereDoYou.setOnClickListener(this)
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Find A Ride",false,true)
    }
    private fun setUpList() {
        for(i in 1..2){
            arrayList.add(TestModel(""))
        }
    }


    private fun setUpAdapter() {
        binding.rvSearch.layoutManager = LinearLayoutManager(activity)
        binding.rvSearch.isNestedScrollingEnabled = false
        adapter = SearchRideAdapter(this,activity, arrayList)
        binding.rvSearch.adapter = adapter
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }


    private fun showDialog() {

        val dialog = activity?.let { Dialog(it, R.style.picture_dialog_style) }
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_select_seat)
        dialog.setCanceledOnTouchOutside(true)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.getWindow()?.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.getWindow()?.setAttributes(lp)

        var tvNext=dialog.findViewById(R.id.tvNext) as TextView
        tvNext.setOnClickListener({
            dialog.dismiss()
            Utils.addNextFragment(activity,CreateRideInstantBookingFragment(),this,false)
        })
    }


    private fun pickDateTime() {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
        val startHour = currentDateTime.get(Calendar.HOUR_OF_DAY)
        val startMinute = currentDateTime.get(Calendar.MINUTE)

        DatePickerDialog(requireContext(), DatePickerDialog.OnDateSetListener { _, year, month, day ->
            TimePickerDialog(requireContext(), TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                val pickedDateTime = Calendar.getInstance()
                pickedDateTime.set(year, month, day, hour, minute)


                val calendar = Calendar.getInstance()
                calendar[year, month] = day

                val format = SimpleDateFormat("MMM dd")
                val strDate: String = format.format(calendar.time)

                binding.tvDate.text = strDate+ "," +hour.toString()+ ":" +minute.toString()
            }, startHour, startMinute, false).show()
        }, startYear, startMonth, startDay).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                val yourLocation = data?.getStringExtra("address")
                binding.tvYourLocation.text = yourLocation
            }else if (requestCode == 300) {
                val whereDoLocation = data?.getStringExtra("address")
                binding.tvWhereDoLocation.text = whereDoLocation
            }
        }
    }

}
