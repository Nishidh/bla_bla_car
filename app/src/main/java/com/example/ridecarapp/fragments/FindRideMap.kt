package com.example.ridecarapp.fragments

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.LocationService.LocationService
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.comman.LocationInterface
import com.example.ridecarapp.databinding.FragmentFindRideMapBinding
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import java.util.*


class FindRideMap : BaseFragment(), OnMapReadyCallback, LocationInterface {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: FragmentFindRideMapBinding
    private lateinit var activity: AppCompatActivity

    private val AUTOCOMPLETE_REQUEST_CODE = 1

    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var type: Int = 0
    private var fragmentName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(arguments != null){
            type = arguments!!.getInt("type")
            if(type == 1){
                fragmentName = "Find A Ride"
            }else{
                fragmentName = "Create A Ride"
            }
            Log.e("TYpe",""+type)
        }
        activity = ApplicationClass.mInstance!!.getactivity()!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_find_ride_map, container, false)
        var view: View = binding.root
        initComponents(view)
        setUpToolBar()
        return view
    }

    override fun initComponents(rootView: View) {
        if (!Places.isInitialized()) {
            Places.initialize(activity, getString(R.string.google_maps_key), Locale.US);
        }
        checkPermissions()
        setOnClickLinster()
    }

    private fun setOnClickLinster() {
        binding.ivNext.setOnClickListener(this)
        binding.etAddress.setOnClickListener(this)
        binding.ivSearch.setOnClickListener(this)
    }

    override fun onSingleClick(rootView: View) {

        when (rootView.id) {
            R.id.ivNext -> {
                if(type == 1){
                    val intent = Intent(context, FindRideFragment::class.java)
                    intent.putExtra("address", binding.etAddress.text.toString())
                    targetFragment!!.onActivityResult(targetRequestCode, RESULT_OK, intent)
                    activity.supportFragmentManager.popBackStack()
                }else if(type == 2) {
                    val intent = Intent(context, CreateRidePickupFragment::class.java)
                    intent.putExtra("address", binding.etAddress.text.toString())
                    targetFragment!!.onActivityResult(targetRequestCode, RESULT_OK, intent)
                    activity.supportFragmentManager.popBackStack()
                   // Utils.addNextFragment(activity, CreateRideDropOfFragment(), this, false)
                }
            }

            R.id.ivSearch -> {
                openAutocomplePlaceApi()
            }
        }
    }

    fun setUpToolBar() {
        (activity as MainActivity).setUpToolBar(fragmentName, true, true)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val cameraPosition =
            CameraPosition.Builder().target(LatLng(latitude, longitude)).zoom(12f).tilt(30f).build()
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

        mMap.setOnCameraIdleListener {
            var latlng: LatLng = mMap.cameraPosition.target
            getAddress(latlng.latitude, latlng.longitude)
        }
    }

    private fun getAddress(latitude: Double, longitude: Double) {

        val geocoder: Geocoder
        val addresses: List<Address>
        geocoder = Geocoder(activity, Locale.getDefault())

        addresses = geocoder.getFromLocation(
            latitude,
            longitude,
            1
        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        if (addresses != null && addresses.size > 0) {
            val address =
                addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

//            val city = addresses[0].locality
//            val state = addresses[0].adminArea
//            val country = addresses[0].countryName
//            val postalCode = addresses[0].postalCode
//            val knownName = addresses[0].featureName

            binding.etAddress.setText(address)
        }


    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            setUpToolBar()
        }
    }

    private fun checkPermissions() {
        if ((ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED) &&
            (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED)
        ) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )) && ActivityCompat.shouldShowRequestPermissionRationale(
                    activity,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            ) {
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ), 1
                )
            } else {
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ), 1
                )
            }
        } else {
            displayLocationSettingsRequest(activity)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(
                            activity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) === PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(
                            activity,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) === PackageManager.PERMISSION_GRANTED)
                    ) {
                        Utils.showToast(activity, "Permission Granted")
                        displayLocationSettingsRequest(activity)
                    }
                } else {

                    Utils.showToast(activity, "Permission Denied")
                }
                return
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        Log.i("TAG", "Place: ${place.name}, ${place.id}")
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.i("TAG", status.statusMessage)
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        } else if (requestCode == 10) {
            startLocationService()
        }
    }

    private fun displayLocationSettingsRequest(context: Context) {
        val googleApiClient = GoogleApiClient.Builder(context)
            .addApi(LocationServices.API).build()
        googleApiClient.connect()
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 10000 / 2.toLong()
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)
        val result: PendingResult<LocationSettingsResult> =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback(object : ResultCallback<LocationSettingsResult?> {
            override
            fun onResult(result: LocationSettingsResult) {
                val status: Status = result.status
                when (status.getStatusCode()) {
                    LocationSettingsStatusCodes.SUCCESS -> {
                        Log.i("SelectLocation", "All location settings are satisfied.")
                        startLocationService()
                    }
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        Log.i(
                            "SelectLocation",
                            "Location settings are not satisfied. Show the user a dialog to upgrade location settings "
                        )
                        try {
                            status.startResolutionForResult(activity, 10)
                        } catch (e: IntentSender.SendIntentException) {
                            Log.i("SelectLocation", "PendingIntent unable to execute request.")
                        }
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                        "SelectLocation",
                        "Location settings are inadequate, and cannot be fixed here. Dialog not created."
                    )
                }
            }
        })
    }


    private fun startLocationService() {
        binding.progressBar.visibility = View.VISIBLE
        val intent = Intent(activity, LocationService::class.java)
        activity.startService(intent)
    }

    override fun onLocationChange(latitude: Double, longitude: Double) {
        if (latitude != 0.0 && longitude != 0.0) {
            binding.progressBar.visibility = View.GONE
            Log.e("Lat", "" + latitude)
            Log.e("Lat", "" + longitude)
            this.latitude = latitude
            this.longitude = longitude
            activity.stopService(Intent(activity, LocationService::class.java))
            val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
            mapFragment?.getMapAsync(this)
        }
    }


    private fun openAutocomplePlaceApi() {
        val fields = listOf(Place.Field.ID, Place.Field.NAME)
        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
            .build(activity)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

}
