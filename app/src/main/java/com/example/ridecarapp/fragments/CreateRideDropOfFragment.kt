package com.example.ridecarapp.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCreateRideDropOfBinding

class CreateRideDropOfFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentCreateRideDropOfBinding




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_ride_drop_of, container, false)
        rootView = binding!!.getRoot()
        activity = ApplicationClass.mInstance!!.getactivity()!!
        setUpToolBar()
        initComponents(rootView)
        return  rootView
    }

    override fun initComponents(rootView: View) {

        setOnClickLinster()
    }
    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Create A Ride",true,true)
    }


    private fun setOnClickLinster() {
        binding.llSearch.setOnClickListener(this)
        binding.btnNext.setOnClickListener(this)
    }

    override fun onSingleClick(rootView: View) {

        when(rootView.id){
            R.id.llSearch->{
                val fragment = FindRideMap()
                val bundle = Bundle()
                bundle.putInt("type",2)
                fragment.arguments = bundle
                fragment.setTargetFragment(this,200)
                Utils.addNextFragment(activity,fragment,this,true)
            }
            R.id.btnNext ->{
                Utils.addNextFragment(activity,CreateRideAddCityFragment(),this,false)
            }
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 200) {
                val address = data?.getStringExtra("address")
                binding.tvLocation.text = address
            }
        }
    }


}
