package com.example.ridecarapp.fragments

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.DatePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.Utils.Utils.Companion.hideKeyboard
import com.cmexpertise.mealkey.mvvm.profile.YourDetailViewModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentYourDetailsBinding
import com.example.ridecarapp.model.your_detail.YourDetailResponseData
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.firebase.iid.FirebaseInstanceId
import com.nishidhpatel.mvvm.imagepicker.setLocalImage
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class YourDetailsFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentYourDetailsBinding
    private val yourDetailViewModel:YourDetailViewModel by viewModel()

    private var mProfileFile: File? = null
    private var imagePath = ""


    private var deviceTokenStr: String? = null
    private var deviceIdStr: String? = null

    private var type: String? = null
    private var token: String? = null

    var list:ArrayList<String> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_your_details, container, false)
        rootView = binding.root
        activity = ApplicationClass.mInstance.getactivity()
        binding.viewModel = yourDetailViewModel
        initComponents(rootView)
        setUpToolBar()
        return rootView
    }

    override fun initComponents(rootView: View) {
        setUpSpinner()
        setOnClickListener()
        setupData()
        setupOberver()
    }

    private fun setupData() {
      val year =   Calendar.getInstance().get(Calendar.YEAR)
        binding.tvYear.text = year.toString()
    }

    private fun setOnClickListener() {
        binding.ivCamera.setOnClickListener(this)
        binding.rlNoThanks.setOnClickListener(this)
        binding.tvYear.setOnClickListener(this)
        binding.flYear.setOnClickListener(this)
    }

    private fun setUpSpinner() {
        list.add("Male")
        list.add("FeMale")
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(activity, R.layout.row_sp_gender, list)
        binding.spGender.adapter=adapter
    }

    override fun onSingleClick(rootView: View) {
        view?.let { activity.hideKeyboard(it) }
        when (rootView.id) {
            R.id.ivCamera -> {
                //var profileImage=ApplicationClass.mInstance?.sharedPreferences?.getString(getString(R.string.PREF_IMAGE), "")
                ImagePicker.with(this)
                    // Crop Square image
                    .cropSquare()
                    .setImageProviderInterceptor { imageProvider -> // Intercept ImageProvider
                        Log.d("ImagePicker", "Selected ImageProvider: " + imageProvider.name)
                    }
                    // Image resolution will be less than 512 x 512
                    .maxResultSize(512, 512)
                    .start(100)
            }

            R.id.rlNoThanks ->{
                if (isValid().equals("")) {
                    doProfileUpdate()
                } else {

                    Utils.showToast(activity, isValid())
                }
            }

            R.id.flYear ->{
                createDialogWithoutDateField()?.show()
            }


        }
    }



    private fun doProfileUpdate() {
        if (Utils.isOnline(activity, true)) {

           // token = ApplicationClass.mInstance?.sharedPreferences?.getString(getString(R.string.PREF_TOKEN), "")
           // var userId = ApplicationClass.mInstance?.sharedPreferences?.getString(getString(R.string.PREF_ID), "")

            var userId = "1"
            var Gender = list.get(binding.spGender.getSelectedItemPosition())

            val userID: RequestBody = userId!!.toRequestBody("text/plain".toMediaTypeOrNull())

            val first_name: RequestBody = binding.etFirstName.text.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val last_name: RequestBody = binding.etLastname.text.toString().toRequestBody("text/plain".toMediaTypeOrNull())

            val gender: RequestBody = Gender.toRequestBody("text/plain".toMediaTypeOrNull())
            val birthYear: RequestBody = binding.tvYear.text.toString().toRequestBody("text/plain".toMediaTypeOrNull())

            val mobile: RequestBody = binding.etMobile.text.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val countryCode: RequestBody = binding.etDialCode.selectedCountryCode.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val email: RequestBody = binding.etEmail.text.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val mini_bio: RequestBody = binding.etBio.text.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            deviceIdStr = Settings.Secure.getString(activity.contentResolver, Settings.Secure.ANDROID_ID)
            deviceTokenStr = FirebaseInstanceId.getInstance().token

            val deviceId: RequestBody = deviceIdStr!!.toRequestBody("text/plain".toMediaTypeOrNull())
            val deviceToken: RequestBody = deviceTokenStr!!.toRequestBody("text/plain".toMediaTypeOrNull())
            val deviceType: RequestBody = "android".toRequestBody("text/plain".toMediaTypeOrNull())

            if (!imagePath.isEmpty() && imagePath != null) {

                val sourceFile = File(imagePath)
                val fileReqBody: RequestBody =
                    sourceFile.asRequestBody("image/*".toMediaTypeOrNull())
                val part: MultipartBody.Part =
                    MultipartBody.Part.createFormData("profile", sourceFile.path, fileReqBody)
                binding.progressBar.visibility = View.VISIBLE

                Log.e(
                    "WithImage",
                    "userId $userId firstname ${binding.etFirstName.text.toString()} lastname ${binding.etLastname.text.toString()} deviceId $deviceIdStr deviceToken $deviceTokenStr deviceType $deviceType "
                )
                yourDetailViewModel.doProfileUpdateWithImage(userID,first_name,last_name,gender,birthYear,mobile,countryCode,email,mini_bio,deviceId,deviceToken,deviceType,part)
            } else {
                Log.e(
                    "WithoutImage",
                    "userId $userId firstname ${binding.etFirstName.text.toString()} lastname ${binding.etLastname.text.toString()} deviceId $deviceIdStr deviceToken $deviceTokenStr deviceType $deviceType "
                )
                binding.progressBar.visibility = View.VISIBLE
                yourDetailViewModel.doProfileUpdateWithoutImage(userID,first_name,last_name,gender,birthYear,mobile,countryCode,email,mini_bio,deviceId,deviceToken,deviceType)
            }
        }
    }

    private fun setupOberver() {

        yourDetailViewModel.showloding.observe(activity, androidx.lifecycle.Observer {
            if (it)
                binding.progressBar.visibility = View.VISIBLE
            else
                binding.progressBar.visibility = View.GONE
        })

        yourDetailViewModel.responseData.observe(activity, androidx.lifecycle.Observer { data ->
            if (data.status == 1) {
                Utils.showToast(activity, "${data.message}")
                saveDataToPreference(data.data)
            } else if(data?.status==0){
                Utils.showToast(activity, data?.message.toString())
            } else if(data?.status==5){
                //(activity as MainActivity).performLogout()
            }
        })

        yourDetailViewModel.showerror.observe(activity, androidx.lifecycle.Observer {

        })
    }

    private fun saveDataToPreference(data: YourDetailResponseData?) {
/*        ApplicationClass.mInstance?.savePreferenceDataBoolean(resources.getString(R.string.PREF_LOGIN), true)
        ApplicationClass.mInstance?.savePreferenceDataBoolean(resources.getString(R.string.PREF_SIGNUP), true)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_ID), data.id)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_NAME), data.name)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_LASTNAME), data.lastName)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_COUNTRYCODE), data.countryCode)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_CREATED_DATE), data.dtCreated)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_UPDATED_DATE), data.dtUpdated)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_TOKEN), data.token)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_MOBILE), data.mobile)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_ISVERIFY), data.isVerified)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_IMAGE), data.image)
        ApplicationClass.mInstance?.savePreferenceDataString(resources.getString(R.string.PREF_OTP), data.otp)*/
    }

/*
    private fun setProfileData() {

        var firstName = ApplicationClass.mInstance?.sharedPreferences?.getString(
            getString(R.string.PREF_NAME),
            ""
        )
        var lastName = ApplicationClass.mInstance?.sharedPreferences?.getString(
            getString(R.string.PREF_LASTNAME),
            ""
        )
        var mobile = ApplicationClass.mInstance?.sharedPreferences?.getString(
            getString(R.string.PREF_MOBILE),
            ""
        )
        var profileImage = ApplicationClass.mInstance?.sharedPreferences?.getString(
            getString(R.string.PREF_IMAGE),
            ""
        )

        binding.etFirstName.setText(firstName)
        binding.etLastname.setText(lastName)
        binding.etMobile.setText(mobile)

        Glide.with(activity)
            .load(profileImage)
            .placeholder(R.drawable.ic_profile_placeholder)
            .into(binding.ivImage)
    }
*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            Log.e("TAG", "Path:${ImagePicker.getFilePath(data)}")
            // File object will not be null for RESULT_OK
            val file = ImagePicker.getFile(data)!!
            when (requestCode) {
                100 -> {
                    mProfileFile = file
                    imagePath = file.absolutePath
                    binding.ivUser.setLocalImage(file, false)
                }
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Utils.showToast(activity, ImagePicker.getError(data))
        } else {
            // Utils.showToast(activity, "Task Cancelled")
        }
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Your Details",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

    fun isValid(): String {
        return if (TextUtils.isEmpty(binding.etFirstName.text.toString())) {
            context!!.getString(R.string.str_enter_firstname)
        } else if (TextUtils.isEmpty(binding.etLastname.text.toString())) {
            context!!.getString(R.string.str_enter_lastname)
        } else if (TextUtils.isEmpty(binding.etMobile.text.toString())) {
            context!!.getString(R.string.str_enter_mobile_number)
        } else if (TextUtils.isEmpty(binding.etEmail.text.toString())) {
            context!!.getString(R.string.str_enter_email)
        } else if (!Utils.isValidEmail(binding.etEmail.text.toString())) {
            context!!.getString(R.string.str_enter_valid_email)
        }  else if (TextUtils.isEmpty(binding.etBio.text.toString())) {
            context!!.getString(R.string.str_enter_bio)
        } else {
            ""
        }
    }

    private fun createDialogWithoutDateField(): DatePickerDialog? {
        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->

            binding.tvYear.text = year.toString()
        }, 2014, 1, 24)
        try {
            val datePickerDialogFields =
                dpd.javaClass.declaredFields
            for (datePickerDialogField in datePickerDialogFields) {
                if (datePickerDialogField.name == "mDatePicker") {
                    datePickerDialogField.isAccessible = true
                    val datePicker = datePickerDialogField[dpd] as DatePicker
                    datePicker.maxDate = Date().getTime()
                    val datePickerFields = datePickerDialogField.type.declaredFields
                    for (datePickerField in datePickerFields) {
                        Log.i("test", datePickerField.name)
                        if ("mDaySpinner" == datePickerField.name) {
                            datePickerField.isAccessible = true
                            val dayPicker = datePickerField[datePicker]
                            (dayPicker as View).visibility = View.GONE
                        }
                    }
                }
            }
        } catch (ex: Exception) {
        }
        return dpd
    }
}

