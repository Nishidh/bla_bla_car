package com.example.ridecarapp.fragments

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCreateRideSelectDateBinding
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class CreateRideSelectDateFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentCreateRideSelectDateBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_ride_select_date, container, false)
        rootView = binding.root
        activity = ApplicationClass.mInstance.getactivity()
        initComponents(rootView)
        setUpToolBar()
        return rootView

    }

    override fun initComponents(rootView: View)
    {
        setOnClickListner()
    }

    private fun setOnClickListner() {
        binding.tvTime.setOnClickListener(this)
        binding.rlNext.setOnClickListener(this)
    }

    override fun onSingleClick(rootView: View) {

        when(rootView.id){
            R.id.tvTime->{
                openTimePickerDialog()
            }
            R.id.rlNext->{
                showDialog()
            }
        }
    }

    private fun openTimePickerDialog() {

        val mTimePicker: TimePickerDialog
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mcurrentTime.get(Calendar.MINUTE)

        mTimePicker = TimePickerDialog(activity, object : TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
                binding.tvTime.text=String.format("%d : %d", hourOfDay, minute)
            }
        }, hour, minute, false)

        mTimePicker.show()
    }

    private fun showDialog() {

        val dialog = activity?.let { Dialog(it, R.style.picture_dialog_style) }
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_select_seat)
        dialog.setCanceledOnTouchOutside(true)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.getWindow()?.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.getWindow()?.setAttributes(lp)

        var tvNext=dialog.findViewById(R.id.tvNext) as TextView
        tvNext.setOnClickListener({
            dialog.dismiss()
            Utils.addNextFragment(activity,CreateRideInstantBookingFragment(),this,false)
        })
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar(Utils.getCurrentDate(),true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }



}
