package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentFilterBinding

class FilterFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentFilterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false)
        rootView = binding!!.getRoot()
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(rootView)
        setUpToolBar()
        return  rootView
    }

    override fun initComponents(rootView: View) {

    }

    override fun onSingleClick(rootView: View) {
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Filter",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }
}
