package com.example.ridecarapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentInBoxRideDetailsBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.CoTravellerAdapter

/**
 * A simple [Fragment] subclass.
 */
class InBoxRideDetailsFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentInBoxRideDetailsBinding

    private var arraylist:ArrayList<TestModel> = ArrayList()
    private lateinit var adapter : CoTravellerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_in_box_ride_details, container, false)
        rootView = binding!!.getRoot()
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(rootView)
        setUpToolBar()
        return  rootView

    }

    override fun initComponents(rootView: View) {

        for(i in 0..2){
            arraylist.add(TestModel(""))
        }

        setupAdapter()

    }

    private fun setupAdapter() {

        binding.rvList.layoutManager=LinearLayoutManager(activity)
        adapter=CoTravellerAdapter(activity,arraylist)
        binding.rvList.adapter=adapter
    }

    override fun onSingleClick(rootView: View) {
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Ride Details",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }
}
