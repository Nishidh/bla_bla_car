package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCancelRideBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.CancelRideAdapter


class CancelRideFragment : BaseFragment() {

    private lateinit var binding: FragmentCancelRideBinding
    private lateinit var activity: AppCompatActivity

    private lateinit var adapter: CancelRideAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cancel_ride, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        return view
    }



    override fun initComponents(view: View) {
        setUpToolBar()
        setUpList()
        setUpAdapter()
    }

    override fun onSingleClick(rootView: View) {
        TODO("Not yet implemented")
    }

    private fun setUpList() {
        for(i in 1..5){
            arrayList.add(TestModel(""))
        }
    }


    private fun setUpAdapter() {
        binding.rvCancelRide.layoutManager = LinearLayoutManager(activity)
        binding.rvCancelRide.isNestedScrollingEnabled = false
        adapter = CancelRideAdapter(this,activity, arrayList)
        binding.rvCancelRide.adapter = adapter
    }


    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Cancel Ride",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

}
