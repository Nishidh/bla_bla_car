package com.example.ridecarapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentInboxBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.InboxAdapter

/**
 * A simple [Fragment] subclass.
 */
class InboxFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding:FragmentInboxBinding

    private var arralist:ArrayList<TestModel> = ArrayList()
    private lateinit var inboxAdapter : InboxAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_inbox, container, false)
        rootView = binding!!.getRoot()
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(rootView)
        return  rootView
    }

    override fun initComponents(rootView: View) {

        setUpAdapter()
    }

    private fun setUpAdapter() {

        for(i in 1..10){
            arralist.add(TestModel(""))
        }

        binding.rvList.layoutManager=LinearLayoutManager(activity)
        inboxAdapter=InboxAdapter(this,activity,arralist)
        binding.rvList.adapter=inboxAdapter
    }

    override fun onSingleClick(rootView: View) {
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Inbox",false,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

    fun setOnItemClick(item: TestModel, pos: Int) {
        Utils.addNextFragment(activity,ChatFragment(),this,false)
    }
}
