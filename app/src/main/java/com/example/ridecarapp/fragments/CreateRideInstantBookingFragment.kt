package com.example.ridecarapp.fragments

import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.view.*
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCreateRideInstantBookingBinding

class CreateRideInstantBookingFragment : BaseFragment() {


    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentCreateRideInstantBookingBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_ride_instant_booking, container, false)
        rootView = binding.root
        activity = ApplicationClass.mInstance.getactivity()
        initComponents(rootView)
        setUpToolBar()
        return rootView
    }

    override fun initComponents(rootView: View) {

        setOnClickListner()
    }

    private fun setOnClickListner() {

        binding.tvYes.setOnClickListener(this)
        binding.tvNo.setOnClickListener(this)
    }

    override fun onSingleClick(rootView: View) {

        when(rootView.id){
            R.id.tvYes,R.id.tvNo->{
                showDialog()
            }
        }
    }

    private fun showDialog() {

        val dialog = activity?.let { Dialog(it, R.style.picture_dialog_style) }
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_confirm_price)
        dialog.setCanceledOnTouchOutside(true)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.getWindow()?.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.getWindow()?.setAttributes(lp)

        var tvTitle=dialog.findViewById(R.id.tvTitle) as TextView
        var btnAgree=dialog.findViewById(R.id.btnAgree) as Button
        var btnDoNotAgree=dialog.findViewById(R.id.btnDoNotAgree) as Button

        btnAgree.setOnClickListener {
            dialog.dismiss()
            Utils.addNextFragment(activity,CreateRideReturnTripFragment(),this,false)
        }
        btnDoNotAgree.setOnClickListener {
            dialog.dismiss()
            Utils.addNextFragment(activity,CreateRideEditPriceFragment(),this,false)
        }

        val text = "<font color=#979191>This is our recommended price per seat.</font> <font color=#FF5252>Is it OK for you?</font>"
        tvTitle.text= Html.fromHtml(text)



    }
    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Instantly Booking",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }
}
