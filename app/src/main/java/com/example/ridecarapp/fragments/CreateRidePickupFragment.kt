package com.example.ridecarapp.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCreateRide2Binding

class CreateRidePickupFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentCreateRide2Binding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_ride2, container, false)
        rootView = binding!!.getRoot()
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(rootView)
        return  rootView
    }

    override fun initComponents(rootView: View) {

        setOnClickLinster()
    }

    private fun setOnClickLinster() {
        binding.llSearch.setOnClickListener(this)
        binding.btnConfirm.setOnClickListener(this)

    }

    override fun onSingleClick(rootView: View) {

        when(rootView.id){
            R.id.llSearch->{
                val fragment = FindRideMap()
                val bundle = Bundle()
                bundle.putInt("type",2)
                fragment.arguments = bundle
                fragment.setTargetFragment(this,200)
                Utils.addNextFragment(activity,fragment,this,true)
            }

            R.id.btnConfirm->{
                Utils.addNextFragment(activity,CreateRideDropOfFragment(),this,true)
            }
        }
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Create A Ride",false,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
            Utils.currentFragment=this
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 200) {
                val address = data?.getStringExtra("address")
                binding.tvPicupAddress.text = address
            }
        }
    }

}
