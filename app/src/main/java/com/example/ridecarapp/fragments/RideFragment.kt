package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.adapter.Pager
import com.example.ridecarapp.databinding.FragmentRideBinding
import com.google.android.material.tabs.TabLayout


class RideFragment : Fragment() , TabLayout.OnTabSelectedListener {

    private var binding: FragmentRideBinding? = null
    private var rootView: View? = null
    private var activity: AppCompatActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = ApplicationClass.mInstance.getactivity()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ride, container, false)
        rootView = binding!!.getRoot()
        initComponents()
        return rootView

    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Rides",false,true)
    }

    private fun initComponents() {

        //Adding the tabs using addTab() method
        binding?.tabLayout?.addTab(binding?.tabLayout?.newTab()!!.setText(""))
        binding?.tabLayout?.addTab(binding?.tabLayout?.newTab()!!.setText(""))
        binding?.tabLayout?.addTab(binding?.tabLayout?.newTab()!!.setText(""))
        binding?.tabLayout?.setTabGravity(TabLayout.GRAVITY_FILL)

        //Creating our pager adapter
        val adapter = Pager(activity?.supportFragmentManager, binding?.tabLayout!!.getTabCount())
        //Adding adapter to pager
        binding?.pager?.setAdapter(adapter)

        //Adding onTabSelectedListener to swipe views
        binding?.tabLayout?.setOnTabSelectedListener(this)

        binding?.tabLayout?.setupWithViewPager(binding?.pager)
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {

    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        binding?.pager?.setCurrentItem(tab!!.getPosition())
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }
}
