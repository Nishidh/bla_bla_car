package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentFindRideDetailBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.ListAdapter

class FindRideDetailFragment : BaseFragment() {


    private lateinit var binding: FragmentFindRideDetailBinding
    private lateinit var activity: AppCompatActivity

    private lateinit var adapter: ListAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_find_ride_detail, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        return view
    }



    override fun initComponents(view: View) {
        setUpToolBar()
        setOnCLickListener()
        setUpList()
        setUpAdapter()
    }

    private fun setOnCLickListener() {
        binding.btnContiue.setOnClickListener(this)
    }

    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.btnContiue -> {
                Utils.addNextFragment(activity,BookedFragment(),this,false)
            }
        }
    }

    private fun setUpList() {
        for(i in 1..5){
            arrayList.add(TestModel(""))
        }
    }


    private fun setUpAdapter() {
        binding.rvList.layoutManager = LinearLayoutManager(activity)
        binding.rvList.isNestedScrollingEnabled = false
        adapter = ListAdapter(this,activity, arrayList)
        binding.rvList.adapter = adapter
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Ride",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }


}
