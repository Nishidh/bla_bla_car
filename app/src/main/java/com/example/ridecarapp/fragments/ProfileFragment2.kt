package com.example.ridecarapp.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentProfile2Binding


class ProfileFragment2 : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentProfile2Binding

    private var isOpenAboutYou=false
    private var isOpenMoney=false
    private var isOpenAboutUs=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile2, container, false)
        rootView = binding!!.getRoot()
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(rootView)
        return  rootView
    }

    override fun initComponents(rootView: View) {

        setOnClickListner()
    }

    private fun setOnClickListner() {
        binding.rlYourDetails.setOnClickListener(this)
        binding.rlCarDetails.setOnClickListener(this)
        binding.rlAboutYou.setOnClickListener(this)
        binding.rlMoney.setOnClickListener(this)
        binding.rlAboutUs.setOnClickListener(this)
    }

    @SuppressLint("NewApi")
    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.rlYourDetails->{
                Utils.addNextFragment(activity,YourDetailsFragment(),this,false)
            }
            R.id.rlCarDetails->{
                Utils.addNextFragment(activity,CarDetailsFragment(),this,false)
            }
            R.id.rlAboutYou->{
                if(isOpenAboutYou){
                    Utils.collapse(binding.llAboutYou)
                    binding.ivAboutYouIcon.setImageDrawable(activity.resources.getDrawable(R.drawable.ic_plusgra,null))
                    isOpenAboutYou=false
                }else{
                    Utils.expand(binding.llAboutYou)
                    binding.ivAboutYouIcon.setImageDrawable(activity.resources.getDrawable(R.drawable.ic_minus_new,null))
                    isOpenAboutYou=true
                }

            }
            R.id.rlMoney->{
                if(isOpenMoney){
                    Utils.collapse(binding.llMoney)
                    binding.ivMoneyIcon.setImageDrawable(activity.resources.getDrawable(R.drawable.ic_plusgra,null))
                    isOpenMoney=false
                }else{
                    Utils.expand(binding.llMoney)
                    binding.ivMoneyIcon.setImageDrawable(activity.resources.getDrawable(R.drawable.ic_minus_new,null))
                    isOpenMoney=true
                }

            }
            R.id.rlAboutUs->{
                if(isOpenAboutUs){
                    Utils.collapse(binding.llAboutUs)
                    binding.ivAboutUs.setImageDrawable(activity.resources.getDrawable(R.drawable.ic_plusgra,null))
                    isOpenAboutUs=false
                }else{
                    Utils.expand(binding.llAboutUs)
                    binding.ivAboutUs.setImageDrawable(activity.resources.getDrawable(R.drawable.ic_minus_new,null))
                    isOpenAboutUs=true
                }

            }
        }

    }
    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("",false,false)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }


}
