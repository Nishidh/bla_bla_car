package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCreateRideAlterBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.CreateRideAdapter

class CreateRideAlterFragment : BaseFragment() {

    private lateinit var binding: FragmentCreateRideAlterBinding
    private lateinit var activity: AppCompatActivity

    private lateinit var adapter: CreateRideAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_ride_alter, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        Utils.currentFragment=this
        initComponents(view)
        return view
    }

    override fun initComponents(view: View) {
        setUpToolBar()
        setUpList()
        setUpAdapter()
    }

    override fun onSingleClick(rootView: View) {

    }

    private fun setUpList() {
        for(i in 1..5){
            arrayList.add(TestModel(""))
        }
    }

    private fun setUpAdapter() {
        binding.rvRide.layoutManager = LinearLayoutManager(activity)
        binding.rvRide.isNestedScrollingEnabled = false
        adapter = CreateRideAdapter(this,activity, arrayList)
        binding.rvRide.adapter = adapter
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBarFiltter("Find A Ride",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
            Utils.currentFragment=this
        }
    }

    fun onItemClick() {
        Utils.addNextFragment(activity,FindRideDetailFragment(),this,false)
    }


}
