package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCreateRideConfirRideBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.ConfirmRideAdapter


class CreateRideConfirRideFragment : BaseFragment() {


    private lateinit var binding: FragmentCreateRideConfirRideBinding
    private lateinit var activity: AppCompatActivity

    private lateinit var adapter: ConfirmRideAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_ride_confir_ride, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        setUpToolBar()
        return view
    }

    override fun initComponents(rootView: View) {
        setUpList()
        setUpAdapter()
        setOnClickLinster()
    }


    override fun onSingleClick(rootView: View) {

        when(rootView.id){
            R.id.rlConfirm->{
                Utils.addNextFragment(activity,CreateRideSelectDateFragment(),this,false)
            }
        }
    }

    private fun setOnClickLinster() {
        binding.rlConfirm.setOnClickListener(this)
    }

    private fun setUpList() {
        for(i in 1..2){
            arrayList.add(TestModel(""))
        }
    }


    private fun setUpAdapter() {
        binding.rvConfirmRide.layoutManager = LinearLayoutManager(activity)
        binding.rvConfirmRide.isNestedScrollingEnabled = false
        adapter = ConfirmRideAdapter(this,activity, arrayList)
        binding.rvConfirmRide.adapter = adapter
    }



    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Confirm Ride",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

    

}
