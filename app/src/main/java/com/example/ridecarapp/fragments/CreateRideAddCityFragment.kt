package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCreateRideAddCityBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.AddCityAdapter

class CreateRideAddCityFragment : BaseFragment() {

    private lateinit var binding: FragmentCreateRideAddCityBinding
    private lateinit var activity: AppCompatActivity

    private lateinit var adapter: AddCityAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_ride_add_city, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        setUpToolBar()
        return view
    }

    override fun initComponents(rootView: View) {
        setUpList()
        setUpAdapter()
        setOnClickListner()
    }

    private fun setOnClickListner() {
        binding.rlNext.setOnClickListener(this)
    }

    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.rlNext->{
                Utils.addNextFragment(activity,CreateRideConfirRideFragment(),this,false)
            }
        }
    }

    private fun setUpList() {
        for(i in 1..2){
            arrayList.add(TestModel(""))
        }
    }


    private fun setUpAdapter() {
        binding.rvAddCity.layoutManager = LinearLayoutManager(activity)
        binding.rvAddCity.isNestedScrollingEnabled = false
        adapter = AddCityAdapter(this,activity, arrayList)
        binding.rvAddCity.adapter = adapter
    }


    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Create A Ride",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

}
