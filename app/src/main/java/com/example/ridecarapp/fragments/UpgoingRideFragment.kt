package com.example.ridecarapp.fragments

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentRideUpgoingBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.UpgoingRideAdapter


class UpgoingRideFragment : BaseFragment() {


    private lateinit var binding: FragmentRideUpgoingBinding
    private lateinit var activity: AppCompatActivity


    private lateinit var adapter: UpgoingRideAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ride_upgoing, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        return view

    }

    override fun initComponents(view: View) {
        setUpList()
        setUpAdapter()
    }

    override fun onSingleClick(rootView: View) {
        TODO("Not yet implemented")
    }

    private fun setUpList() {
        for(i in 1..5){
            arrayList.add(TestModel(""))
        }
    }


    private fun setUpAdapter() {
        binding.rvUpgoingRide.layoutManager = LinearLayoutManager(activity)
        binding.rvUpgoingRide.isNestedScrollingEnabled = false
        adapter = UpgoingRideAdapter(this,activity, arrayList)
        binding.rvUpgoingRide.adapter = adapter
    }

    fun OnItemClick() {
        Utils.addNextFragment(activity,RideDetailFragment(),this,false)
    }


    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Ride",false,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Utils.setStatusBarColor(activity, ContextCompat.getColor(activity,R.color.white))
            }
        }
    }

    fun OnItemProfileClick() {
        Utils.addNextFragment(activity,ProfileFragment(),this,false)
    }

}
