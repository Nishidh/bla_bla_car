package com.example.ridecarapp.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Constans_vehicle_brand_id
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentVehicleModelBinding
import com.example.ridecarapp.model.vehiclemodel.VehicleModelData
import com.example.ridecarapp.mvvm.login_new.VehicleModelViewModel
import com.kinokeEvent.kinoke.mvvm.otherAlbums.VehicleModelAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel


class VehicleModelFragment : BaseFragment() {

    private lateinit var binding: FragmentVehicleModelBinding
    private lateinit var activity: AppCompatActivity

    private val vehicleModelViewModel : VehicleModelViewModel by viewModel()

    private lateinit var adapter: VehicleModelAdapter
    private val arrayList: ArrayList<VehicleModelData> = ArrayList()

    private var id:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id=it.getString(Constans_vehicle_brand_id)
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_vehicle_model, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        binding.model=vehicleModelViewModel
        initComponents(view)
        setUpToolBar()
        return view
    }


    override fun initComponents(rootView: View) {
        callApi()
        setUpObserver()


        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var list=arrayList.filter { it.models.contains(s.toString(),true) }
                adapter.setList(list)
            }

        })
    }

    private fun setUpObserver() {
        vehicleModelViewModel.responseData.observe(this, Observer { data ->
            binding.progressBar.visibility=View.GONE
            if(data.status==1){
                arrayList.addAll(data.data)
                setUpAdapter()
            }else{
                Utils.showToast(activity,data.message)
            }
        })


        vehicleModelViewModel.showerror.observe(this, Observer {

        })
    }


    override fun onSingleClick(rootView: View) {
    }


    private fun setUpAdapter() {
        binding.rvVehicleModel.layoutManager = LinearLayoutManager(activity)
        binding.rvVehicleModel.isNestedScrollingEnabled = false
        adapter = VehicleModelAdapter(this,activity, arrayList)
        binding.rvVehicleModel.adapter = adapter
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Vehicle's Model",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

    fun setOnItemClick(item: VehicleModelData) {
        Utils.addNextFragment(activity,VehicleTypeFragment(),this,false)
    }

    private fun callApi() {
        if (Utils.isOnline(activity, true)) {
            binding.progressBar.visibility = View.VISIBLE
            vehicleModelViewModel.getModelList(id)
        }
    }


}
