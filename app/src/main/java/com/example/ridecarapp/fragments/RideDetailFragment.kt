package com.example.ridecarapp.fragments

import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentRideDetailBinding


class RideDetailFragment : BaseFragment() {

    private lateinit var binding: FragmentRideDetailBinding
    private lateinit var activity: AppCompatActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ride_detail, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        return view
    }

    override fun initComponents(rootView: View) {
        setUpToolBar()
        setOnClickListener()
    }

    private fun setOnClickListener() {
        binding.btnManageBooking.setOnClickListener(this)
    }

    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.btnManageBooking -> {
                showDialog()
            }
        }
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Ride Details",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

    private fun showDialog() {

        val dialog = activity?.let { Dialog(it, R.style.picture_dialog_style) }
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_ride)
        dialog.setCanceledOnTouchOutside(true)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.getWindow()?.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.getWindow()?.setAttributes(lp)

        val reportRide = dialog.findViewById<RelativeLayout>(R.id.rlReportRide)
        val cancelRide = dialog.findViewById<RelativeLayout>(R.id.rlCancelRide)

        reportRide.setOnClickListener {
            Utils.addNextFragment(activity,ReportRideFragment(),this,false)
            dialog.dismiss()
        }

        cancelRide.setOnClickListener {
            Utils.addNextFragment(activity,CancelRideFragment(),this,false)
            dialog.dismiss()
        }

    }


}
