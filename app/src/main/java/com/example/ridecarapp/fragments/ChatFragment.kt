package com.example.ridecarapp.fragments

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentChatBinding
import com.example.ridecarapp.model.ChatMessage
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.kinokeEvent.kinoke.mvvm.otherAlbums.ChatAdapter

/**
 * A simple [Fragment] subclass.
 */
class ChatFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentChatBinding

    private var list:ArrayList<ChatMessage> = ArrayList()
    private lateinit var chatAdapter : ChatAdapter

    private var userId:String="2"
    private var senderId:String="1"
    private var databaseReferenceChat :String=""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false)
        rootView = binding!!.getRoot()
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(rootView)
        setUpToolBar()
        return  rootView
    }

    override fun initComponents(rootView: View) {

        if(userId.toInt() > senderId.toInt()){
            databaseReferenceChat=senderId+"_"+userId
        }else{
            databaseReferenceChat=userId+"_"+senderId
        }

        scrollToTopOfKeyboard()
        setOnClickListner()
        getChatData()
    }

    private fun getChatData() {

        getChatNodes().child(databaseReferenceChat).addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                list.clear()
                var iter : Iterator<DataSnapshot> = dataSnapshot.children.iterator()
                if(iter.hasNext()){
                    while (iter.hasNext()){

                        val chatMessage = iter.next().getValue(ChatMessage::class.java)
                        if(chatMessage?.senderId.equals(userId,true)){
                            chatMessage?.isSender=true
                        }else{
                            chatMessage?.isSender=false
                        }
                        list.add(chatMessage!!)

                    }
                    setUpAdapter()
                    val totalCount: Int = chatAdapter.getItemCount()
                    binding.rvList.scrollToPosition(totalCount - 1)
                }

            }

        })
    }

    private fun setOnClickListner() {
        binding.ivInfo.setOnClickListener(this)
        binding.ivSend.setOnClickListener(this)
    }

    private fun setUpAdapter() {

        binding.rvList.layoutManager=LinearLayoutManager(activity)
        chatAdapter=ChatAdapter(activity,list)
        binding.rvList.adapter=chatAdapter
    }

    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.ivInfo->{
                Utils.addNextFragment(activity,InBoxRideDetailsFragment(),this,false)
            }
            R.id.ivSend->{
                sendMessage()
            }
        }
    }

    private fun sendMessage() {
        if(binding.etMessage.text.toString().isEmpty()){
            Utils.showToast(activity,"Please enter message first")
        }else{
            addMessageToConversionNode()
        }
    }

    private fun addMessageToConversionNode() {
        var chatMessage=ChatMessage()
        chatMessage.msg=binding.etMessage.text.toString()
        chatMessage.senderId=userId
        chatMessage.timeStemp=System.currentTimeMillis().toString()

        var pusKey:String= getChatNodes().push().key!!

        getChatNodes().child(databaseReferenceChat)
            .child(pusKey)
            .setValue(chatMessage)
            .addOnSuccessListener {
                binding.etMessage.text = null

            }

        //Utils.hideKeyboard(getActivity());
        val count: Int = chatAdapter.getItemCount()
        binding.rvList.scrollToPosition(count - 1)


    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Chat",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }
    private fun scrollToTopOfKeyboard() {
        if (Build.VERSION.SDK_INT >= 11) {
            binding.rvList.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
                if (bottom < oldBottom) {
                    binding.rvList.postDelayed({
                        binding.rvList.smoothScrollToPosition(
                            binding.rvList.adapter!!.itemCount - 1
                        )
                    }, 100)
                }
            }
        }
    }
}
