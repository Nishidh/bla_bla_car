package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentBookedBinding

class BookedFragment : BaseFragment() {

    private lateinit var binding: FragmentBookedBinding
    private lateinit var activity: AppCompatActivity


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_booked, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        return view
    }


    override fun initComponents(view: View) {
        setUpToolBar()
        setOnCLickListener()

    }

    private fun setOnCLickListener() {
        binding.btnOk.setOnClickListener(this)
    }

    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.btnOk -> {
                //Utils.addNextFragment(activity,BookedFragment(),this,false)
            }
        }
    }


    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Booked",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }
}
