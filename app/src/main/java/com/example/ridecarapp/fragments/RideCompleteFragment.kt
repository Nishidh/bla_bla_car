package com.example.ridecarapp.fragments

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentRideCompleteBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.CompletedRideAdapter


class RideCompleteFragment : BaseFragment() {


    private lateinit var binding: FragmentRideCompleteBinding
    private lateinit var activity: AppCompatActivity


    private lateinit var adapter: CompletedRideAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ride_complete, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        return view

    }

    override fun initComponents(view: View) {
        setUpList()
        setUpAdapter()
    }

    override fun onSingleClick(rootView: View) {
        TODO("Not yet implemented")
    }

    private fun setUpList() {
        for(i in 1..5){
            arrayList.add(TestModel(""))
        }
    }


    private fun setUpAdapter() {
        binding.rvRideComplete.layoutManager = LinearLayoutManager(activity)
        binding.rvRideComplete.isNestedScrollingEnabled = false
        adapter = CompletedRideAdapter(this,activity, arrayList)
        binding.rvRideComplete.adapter = adapter
    }

    fun OnItemClick() {
        Utils.addNextFragment(activity,RatingFragment(),this,false)
    }


    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Ride",false,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Utils.setStatusBarColor(activity, ContextCompat.getColor(activity,R.color.white))
            }
        }
    }

    fun onItemProfileClick() {
        Utils.addNextFragment(activity,ProfileFragment(),this,false)
    }

}
