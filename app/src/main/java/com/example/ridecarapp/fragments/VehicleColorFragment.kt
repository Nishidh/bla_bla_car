package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentVehicleColorBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.VehicleColorAdapter


class VehicleColorFragment : BaseFragment() {

    private lateinit var binding: FragmentVehicleColorBinding
    private lateinit var activity: AppCompatActivity

    private lateinit var adapter: VehicleColorAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_vehicle_color, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        initComponents(view)
        setUpToolBar()
        return view
    }

    override fun initComponents(rootView: View) {
        setUpList()
        setUpAdapter()
    }

    override fun onSingleClick(rootView: View) {
    }

    private fun setUpList() {
        for(i in 1..10){
            arrayList.add(TestModel(""))
        }
    }

    private fun setUpAdapter() {
        binding.rvVehicleColor.layoutManager = GridLayoutManager(getActivity(), 3)
        binding.rvVehicleColor.isNestedScrollingEnabled = false
        adapter = VehicleColorAdapter(this,activity, arrayList)
        binding.rvVehicleColor.adapter = adapter
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Vehicle Color",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

    fun setOnItemClick() {
        Utils.addNextFragment(activity,VehiclePhotoFragment(),this,false)
    }


}
