package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.FragmentHomeBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener


class HomeFragment : BaseFragment() {

    private var binding: FragmentHomeBinding? = null
    private var rootView: View? = null
    private var activity: AppCompatActivity? = null


    private lateinit var rideFragment:Fragment
    private lateinit var searchFragment:Fragment
    private lateinit var createRideFragment:Fragment
    private lateinit var inboxFragment:Fragment
    private lateinit var profile:Fragment
    private lateinit var active: Fragment

    private lateinit var fragmentManger: FragmentManager
    private var itemIndex = 0



    private val tabIcons = intArrayOf(
        R.drawable.menu_ride_selector,
        R.drawable.menu_search_selector,
        R.drawable.menu_create_selector,
        R.drawable.menu_inbox_selector,
        R.drawable.menu_profile_selector
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = ApplicationClass.mInstance.getactivity()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        rootView = binding!!.getRoot()
        initComponents(rootView!!)
        return rootView
    }

    override fun initComponents(rootView: View) {
        setTabIcon()
        setUpFragmentManager()

        binding!!.fragmentMainTlHome.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {
                    changeFragment(0,rideFragment)
                } else if (tab.position == 1) {
                    changeFragment(1,searchFragment)
                } else if (tab.position == 2) {
                    changeFragment(2,createRideFragment)
                } else if (tab.position == 3) {
                    changeFragment(3,inboxFragment)
                }else if (tab.position == 4) {
                    changeFragment(4,profile)
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

    }

    private fun setUpFragmentManager() {
        fragmentManger=activity!!.supportFragmentManager


        rideFragment= RideFragment()
        searchFragment=FindRideFragment()
        createRideFragment=CreateRidePickupFragment()
        inboxFragment=InboxFragment()
        profile=ProfileFragment2()


        fragmentManger.beginTransaction().add(R.id.frameLayout, rideFragment, "1").commit()
        fragmentManger.beginTransaction().add(R.id.frameLayout, searchFragment, "2").hide(searchFragment).commit()
        fragmentManger.beginTransaction().add(R.id.frameLayout, createRideFragment, "3").hide(createRideFragment).commit()
        fragmentManger.beginTransaction().add(R.id.frameLayout, inboxFragment, "4").hide(inboxFragment).commit()
        fragmentManger.beginTransaction().add(R.id.frameLayout, profile, "5").hide(profile).commit()
        active=rideFragment
        changeFragment(0,rideFragment)



    }

    override fun onSingleClick(rootView: View) {

    }


    private fun setTabIcon() {
        binding!!.fragmentMainTlHome.addTab(
            binding!!.fragmentMainTlHome.newTab().setIcon(tabIcons[0]).setText("Ride")
        )
        binding!!.fragmentMainTlHome.addTab(
            binding!!.fragmentMainTlHome.newTab().setIcon(tabIcons[1]).setText("Search"))
        binding!!.fragmentMainTlHome.addTab(
            binding!!.fragmentMainTlHome.newTab().setIcon(tabIcons[2]).setText("Create")
        )
        binding!!.fragmentMainTlHome.addTab(
            binding!!.fragmentMainTlHome.newTab().setIcon(tabIcons[3]).setText("Inbox")
        )

        binding!!.fragmentMainTlHome.addTab(
            binding!!.fragmentMainTlHome.newTab().setIcon(tabIcons[4]).setText("Profile")
        )
    }

    private fun changeFragment(itemIndex: Int, fragment: Fragment) {
        val fragmentTransaction = fragmentManager!!.beginTransaction()
        if (this.itemIndex > itemIndex)
            fragmentTransaction.setCustomAnimations(R.animator.slide_fragment_horizontal_left_in, R.animator.slide_fragment_horizontal_right_out)
        else
            fragmentTransaction.setCustomAnimations(R.animator.slide_fragment_horizontal_right_in, R.animator.slide_fragment_horizontal_left_out
            )
        fragmentTransaction.hide(active).show(fragment).commit()
        this.itemIndex = itemIndex
        active = fragment
        Utils.currentFragment=active

    }

}
