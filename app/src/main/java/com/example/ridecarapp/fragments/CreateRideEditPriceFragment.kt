package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCreateRideEditPriceBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.EditPriceAdapter

/**
 * A simple [Fragment] subclass.
 */
class CreateRideEditPriceFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentCreateRideEditPriceBinding

    private var list:ArrayList<TestModel> = ArrayList()
    private lateinit var adapter:EditPriceAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_ride_edit_price, container, false)
        rootView = binding.root
        activity = ApplicationClass.mInstance.getactivity()
        initComponents(rootView)
        setUpToolBar()
        return rootView

    }

    override fun initComponents(rootView: View) {

        for(i in 0..5){
            list.add(TestModel(""))
        }
        setUpAdapter()
        setOnClickLisnter()
    }

    private fun setOnClickLisnter() {
        binding.rlConfirm.setOnClickListener(this)
    }

    private fun setUpAdapter() {

        binding.rvList.layoutManager=LinearLayoutManager(activity)
        adapter=EditPriceAdapter(activity,list)
        binding.rvList.adapter=adapter
    }

    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.rlConfirm->{
                Utils.addNextFragment(activity,CreateRideReturnTripFragment(),this,false)
            }
        }
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Edit Price",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }
}
