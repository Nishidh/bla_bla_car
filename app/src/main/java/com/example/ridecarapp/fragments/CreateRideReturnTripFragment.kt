package com.example.ridecarapp.fragments

import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCreateRideReturnTripBinding


class CreateRideReturnTripFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentCreateRideReturnTripBinding




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_ride_return_trip, container, false)
        rootView = binding.root
        activity = ApplicationClass.mInstance.getactivity()
        initComponents(rootView)
        setUpToolBar()
        return rootView
    }

    override fun initComponents(rootView: View) {

        setOnClickListner()
    }

    private fun setOnClickListner() {
        binding.rlNoThanks.setOnClickListener(this)
    }

    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.rlNoThanks->{
                showDialog()
            }
        }
    }
    private fun showDialog() {

        val dialog = activity?.let { Dialog(it, R.style.picture_dialog_style) }
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_write_about_ride)
        dialog.setCanceledOnTouchOutside(true)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.getWindow()?.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.getWindow()?.setAttributes(lp)

        var btnSubmit=dialog.findViewById(R.id.btnSubmit) as Button
        btnSubmit.setOnClickListener{
            dialog.dismiss()
            Utils.addNextFragment(activity,CreateOnlineRideFragment(),this,false)
        }


    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Return Trip",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

}
