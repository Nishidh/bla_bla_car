package com.example.ridecarapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentProfileBinding
import com.kinokeEvent.kinoke.mvvm.otherAlbums.VehiclePhotoAdapter

class ProfileFragment : BaseFragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var activity: AppCompatActivity

    private lateinit var adapter: VehiclePhotoAdapter
    private val arrayList: ArrayList<TestModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        com.cmexpertise.mealkey.Utils.Utils.setStatusBarColor(activity,ContextCompat.getColor(activity,R.color.button_bg))
        initComponents(view)
        return view
    }

    override fun initComponents(view: View) {
        setUpToolBar()
        setUpList()
        setUpAdapter()
        setOnClickLisnter()
    }

    private fun setOnClickLisnter() {
        binding.ivBack.setOnClickListener(this)
    }

    override fun onSingleClick(rootView: View) {
        when(rootView.id){
            R.id.ivBack->{
                activity.supportFragmentManager.popBackStack()
            }
        }
    }

    private fun setUpList() {
        for(i in 1..5){
            arrayList.add(TestModel(""))
        }
    }

    private fun setUpAdapter() {
        binding.rvVehiclePhoto.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL ,false)
        binding.rvVehiclePhoto.isNestedScrollingEnabled = false
        adapter = VehiclePhotoAdapter(activity, arrayList)
        binding.rvVehiclePhoto.adapter = adapter
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Profile",false,false)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }


}
