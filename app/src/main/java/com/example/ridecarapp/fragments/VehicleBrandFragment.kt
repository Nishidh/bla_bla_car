package com.example.ridecarapp.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.cmexpertise.mealkey.Utils.Constans_vehicle_brand_id
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentVehicleBrandBinding
import com.example.ridecarapp.model.vehiclebrand.VehicleData
import com.example.ridecarapp.mvvm.login_new.VehicleBrandViewModel
import com.kinokeEvent.kinoke.mvvm.otherAlbums.VehicleBrandAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class VehicleBrandFragment : BaseFragment() {

    private lateinit var binding: FragmentVehicleBrandBinding
    private lateinit var activity: AppCompatActivity

    private val vehicleBrandViewModel : VehicleBrandViewModel by viewModel()

    private lateinit var adapter: VehicleBrandAdapter
    private val arrayList: ArrayList<VehicleData> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_vehicle_brand, container, false)
        var view: View = binding.root
        activity = ApplicationClass.mInstance!!.getactivity()!!
        binding.model=vehicleBrandViewModel
        initComponents(view)
        setUpToolBar()
        return view
    }

    override fun initComponents(rootView: View) {
        callApi()
        setUpObserver()

        binding.etSearch.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var list=arrayList.filter { it.brandName.contains(s.toString(),true) }
                adapter.setList(list)
            }

        })
    }

    private fun setUpObserver() {
        vehicleBrandViewModel.responseData.observe(this, Observer { data ->
            binding.progressBar.visibility=View.GONE
            if(data.status==1){
                arrayList.addAll(data.data)
                setUpAdapter()
            }else{
                Utils.showToast(activity,data.message)
            }
        })


        vehicleBrandViewModel.showerror.observe(this, Observer {

        })
    }

    override fun onSingleClick(rootView: View) {
    }

    private fun setUpAdapter() {
        binding.rvVehicleBrand.layoutManager = LinearLayoutManager(activity)
        binding.rvVehicleBrand.isNestedScrollingEnabled = false
        adapter = VehicleBrandAdapter(this,activity, arrayList)
        binding.rvVehicleBrand.adapter = adapter
    }
    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Vehicle's Brand",true,true)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }

    fun setOnItemClick(item: VehicleData) {
        var fragment=VehicleModelFragment()
        var bundle= Bundle().apply { putString(Constans_vehicle_brand_id,item.id) }
        fragment.arguments=bundle
        Utils.addNextFragment(activity,fragment,this,false)
    }

    private fun callApi() {
        if (Utils.isOnline(activity, true)) {
            binding.progressBar.visibility = View.VISIBLE
            vehicleBrandViewModel.getBrandList()
        }
    }

}
