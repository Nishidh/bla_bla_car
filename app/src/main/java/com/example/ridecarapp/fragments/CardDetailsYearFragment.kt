package com.example.ridecarapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cmexpertise.beautyapp.fragment.BaseFragment
import com.example.ridecarapp.ApplicationClass

import com.example.ridecarapp.R
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.databinding.FragmentCardDetailsYearBinding

/**
 * A simple [Fragment] subclass.
 */
class CardDetailsYearFragment : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var activity: AppCompatActivity
    private lateinit var binding: FragmentCardDetailsYearBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_card_details_year, container, false)
        rootView = binding.root
        activity = ApplicationClass.mInstance.getactivity()
        initComponents(rootView)
        setUpToolBar()
        return rootView

    }

    override fun initComponents(rootView: View) {

        setUpYearAdapter()
    }

    override fun onSingleClick(rootView: View) {

    }
    private fun setUpYearAdapter() {

        var list:ArrayList<String> = ArrayList()
        list.add("2021")

        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(activity, R.layout.row_sp_country, list)
        binding.spYear.adapter=adapter
    }

    fun setUpToolBar(){
        (activity as MainActivity).setUpToolBar("Car Details",true,true)
    }


    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            setUpToolBar()
        }
    }
}
