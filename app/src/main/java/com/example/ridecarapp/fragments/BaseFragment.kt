package com.cmexpertise.beautyapp.fragment

import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.ridecarapp.R
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


abstract class BaseFragment : Fragment(), View.OnClickListener {

    private var mLastClickTime: Long = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_base, container, false)
    }


    abstract fun initComponents(rootView: View)

    abstract fun onSingleClick(rootView: View)


    override fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < MAX_CLICK_INTERVAL) {

            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        onSingleClick(v)
    }

/*
    fun validateField(container: View, etFielde: EditText, errMsg: String) {
        activity?.let { Utils.snackBar(container, errMsg, true, it) }
        etFielde.requestFocus()
        //requestFocus(etFielde);

    }*/

    fun handleDoubleClick() {
        /**
         * Logic to Prevent the Launch of the Fragment Twice if User makes
         * the Tap(Click) very Fast.
         */
        if (SystemClock.elapsedRealtime() - mLastClickTime < MAX_CLICK_INTERVAL) {

            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
    }

    companion object {
        private val MAX_CLICK_INTERVAL = 700
    }

    open fun getChatNodes(): DatabaseReference {
        return FirebaseDatabase.getInstance().reference.child("Chats")
    }


}
