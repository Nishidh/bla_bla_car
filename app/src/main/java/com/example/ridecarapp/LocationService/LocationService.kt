package com.example.ridecarapp.LocationService

import android.Manifest
import android.app.Service
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.cmexpertise.mealkey.Utils.LATTITUDE
import com.cmexpertise.mealkey.Utils.LONGITUDE
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.activities.MainActivity
import com.example.ridecarapp.comman.LocationInterface
import com.example.ridecarapp.fragments.FindRideMap
import com.google.android.gms.location.*

class LocationService : Service() {

    private val TAG:String="LocationService"

    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private val INTERVAL: Long = 2000
    private val FASTEST_INTERVAL: Long = 1000
    lateinit var mLastLocation: Location
    internal lateinit var mLocationRequest: LocationRequest
    private val REQUEST_PERMISSION_LOCATION = 10

    private lateinit var activity: AppCompatActivity
    lateinit var locationInterface: LocationInterface


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG,"OnCreate")

        activity = ApplicationClass.mInstance!!.getactivity()!!
        if(Utils.currentFragment is FindRideMap){

            var fragment : FindRideMap = Utils.currentFragment as FindRideMap
            locationInterface = (fragment) as LocationInterface
        }

        mLocationRequest = LocationRequest()
        startLocationUpdates()

    }

    protected fun startLocationUpdates() {

        // Create the location request to start receiving updates

        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest!!.setInterval(INTERVAL)
        mLocationRequest!!.setFastestInterval(FASTEST_INTERVAL)

        // Create LocationSettingsRequest object using location request
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        val locationSettingsRequest = builder.build()

        val settingsClient = LocationServices.getSettingsClient(activity)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity)
        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        mFusedLocationProviderClient!!.requestLocationUpdates(mLocationRequest, mLocationCallback,
            Looper.myLooper())
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            // do work here
            locationResult.lastLocation
            onLocationChanged(locationResult.lastLocation)
        }
    }

    fun onLocationChanged(location: Location) {
        // New location has now been determined

        mLastLocation = location
        Log.d(TAG,"${mLastLocation.latitude}")
        Log.d(TAG,"${mLastLocation.longitude}")

        LATTITUDE = mLastLocation.latitude
        LONGITUDE = mLastLocation.longitude

        locationInterface.onLocationChange(mLastLocation.latitude,mLastLocation.longitude)
        // You can now create a LatLng Object for use with maps
    }

    private fun stoplocationUpdates() {
        mFusedLocationProviderClient!!.removeLocationUpdates(mLocationCallback)
    }

    override fun onDestroy() {
        super.onDestroy()
        stoplocationUpdates()
    }
}