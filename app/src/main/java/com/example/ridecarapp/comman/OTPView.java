package com.example.ridecarapp.comman;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.example.ridecarapp.R;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.shape.CornerFamily;

import java.util.ArrayList;

public class OTPView extends LinearLayout
{
    StringBuilder builder;
    ArrayList<EditText> textList;
    ArrayList<MaterialCardView> cardViewList;
    int etCount,etInputType,viewType,lineColor,texttype,textSize;
    OTPListener otpListener;

    public OTPView(@NonNull Context context) {
        super(context,null, R.attr.otpViewStyle);
        createOTpView(context);
    }

    public OTPView(@NonNull Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.OTPView,0,0);

        int size = (int) getResources().getDimension(R.dimen._9sdp);
        textSize = typedArray.getDimensionPixelSize(R.styleable.OTPView_setTextSize,size);
        etCount = typedArray.getInteger(R.styleable.OTPView_setCount,6);
        etInputType = typedArray.getInteger(R.styleable.OTPView_inputType, InputType.TYPE_CLASS_NUMBER);
        viewType = typedArray.getInteger(R.styleable.OTPView_viewType,0);
        lineColor = typedArray.getInteger(R.styleable.OTPView_etLineColor,1);
        texttype = typedArray.getInteger(R.styleable.OTPView_textStyle,0);

        setGravity(Gravity.CENTER);
        setOrientation(VERTICAL);
        typedArray.recycle();
        createOTpView(context);
    }

    public void setOtpListener(OTPListener otpListener) {
        this.otpListener = otpListener;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public int getEtInputType() {
        return etInputType;
    }

    public void setEtInputType(int etInputType) {
        this.etInputType = etInputType;
    }

    public int getEtCount() {
        return etCount;
    }

    public void setEtCount(int etCount) {
        this.etCount = etCount;
    }

    public void onOtpCompletionListener(OTPListener otpListener){
        this.otpListener = otpListener;
    }

    @SuppressLint("ResourceType")
    private void createOTpView(final Context context)
    {
        final InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        final LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(Gravity.CENTER);
        final LayoutParams layoutParams = new LayoutParams( (int) context.getResources().getDimension(R.dimen._34sdp),
                (int) context.getResources().getDimension(R.dimen._64sdp));
        layoutParams.leftMargin = (int) context.getResources().getDimension(R.dimen._15sdp);

        LinearLayout etLinear = new LinearLayout(context);

        etLinear.setOrientation(LinearLayout.HORIZONTAL);
        etLinear.setGravity(Gravity.CENTER);

        textList = new ArrayList<>();
        cardViewList = new ArrayList<>();

        for (int i = 1 ; i <= etCount ; i++)
        {
            MaterialCardView cardView = new MaterialCardView(context);
            cardView.setShapeAppearanceModel(
                    cardView.getShapeAppearanceModel()
                            .toBuilder()
                            .setAllCorners(CornerFamily.CUT,28)
                            .build());

            cardView.setMinimumHeight(140);
            cardView.setMinimumWidth(80);

            EditText editText = new EditText(context);

            editText.setTextSize(textSize);
            if (texttype == 1)
                editText.setTypeface(Typeface.DEFAULT_BOLD);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                editText.setTextAlignment(TEXT_ALIGNMENT_CENTER);
            }

            editText.setTextColor(Color.BLACK);

            if(etInputType == 0){
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            }
            else{
                editText.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED| InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                editText.setTransformationMethod(new PasswordTransformationMethod());
                editText.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
            }
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            editText.setEnabled(false);
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});

            editText.setBackground(null);

            cardView.addView(editText);
            cardView.setStrokeWidth(2);

            cardView.setStrokeColor(ContextCompat.getColor(context,R.color.gray));
            cardView.setCardElevation(0);

            CardView.LayoutParams cardLayoutParams = new FrameLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._134sdp),
                    (int) context.getResources().getDimension(R.dimen._134sdp));
            cardLayoutParams.setMargins(12, 0, 12, 0);
            cardLayoutParams.gravity = Gravity.CENTER;

            cardView.setLayoutParams(cardLayoutParams);

            etLinear.addView(cardView,layoutParams);
            textList.add(editText);
            cardViewList.add(cardView);
        }

        builder = new StringBuilder();
        textList.get(0).setEnabled(true);

        for (int i = 0 ; i < textList.size() ; i++)
        {
            final int a = i;

            textList.get(a).setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction()!= KeyEvent.ACTION_DOWN)
                        return true;

                    if(keyCode == KeyEvent.KEYCODE_DEL){

                        if(a != 0){
                            if(textList.get(a).getText().toString().isEmpty())
                            {
                                textList.get(a-1).setEnabled(true);
                                textList.get(a-1).setText("");
                                textList.get(a-1).requestFocus();
                                textList.get(a).setEnabled(false);
                                cardViewList.get(a).setStrokeColor(ContextCompat.getColor(context,R.color.colorAccent));
                            }else {
                                textList.get(a).setEnabled(true);
                                textList.get(a).setText("");
                                textList.get(a).requestFocus();
                            }
                        }
                        else {
                            textList.get(a).setText("");
                            textList.get(a).setEnabled(true);
                            textList.get(a).clearFocus();
                            cardViewList.get(a).setStrokeColor(ContextCompat.getColor(context,R.color.colorAccent));
                            assert imm != null;
                            imm.hideSoftInputFromWindow(linearLayout.getWindowToken(), 0);
                        }
                    }
                    return true;
                }
            });
            textList.get(a).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(s.length() != 0){
                        builder.append(s);
                    }else{
                        builder.delete(a,builder.length());
                    }
                    if(builder.length() == textList.size()) {
                        otpListener.onOTPComplete(builder.toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    String s1 = s.toString();
                    if(a == textList.size()-1 && !s1.isEmpty())
                    {
                        textList.get(a).clearFocus();
                        cardViewList.get(a).setStrokeColor(ContextCompat.getColor(context,R.color.colorAccent));
                        assert imm != null;
                        imm.hideSoftInputFromWindow(linearLayout.getWindowToken(), 0);
                    }
                    else if (!s1.isEmpty())
                    {
                        textList.get(a+1).setEnabled(true);
                        textList.get(a+1).requestFocus();
                        textList.get(a).setEnabled(false);
                        cardViewList.get(a).setStrokeColor(ContextCompat.getColor(context,R.color.colorAccent));
                    }
                }
            });
        }

        linearLayout.addView(etLinear);

        /*LinearLayout linearLayout1 = new LinearLayout(context);
        linearLayout1.setOrientation(LinearLayout.VERTICAL);
        linearLayout1.setGravity(Gravity.CENTER);

        Button button = new Button(context);
        button.setAllCaps(false);
        button.setText("Get OTP");
        linearLayout1.addView(button,new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayout.addView(linearLayout1);*/

        /*builder = new StringBuilder();

        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                builder.delete(0,builder.length());

                if(builder.length() <= textList.size()){
                    for(int i = 0 ; i < textList.size() ; i++){
                        if(!textList.get(i).getText().toString().isEmpty()){
                            builder.append(textList.get(i).getText());
                        }
                    }
                }

                if(builder.length() < etCount || builder.length() > etCount){
                    Toast.makeText(context, "Wrong OTP entered...!", Toast.LENGTH_SHORT).show();
                }else if(builder.length() == etCount){
                    Toast.makeText(context, "OTP is : "+builder, Toast.LENGTH_SHORT).show();
                }
            }
        });*/
        addView(linearLayout);
    }
}

