package com.example.ridecarapp.comman

interface LocationInterface {
    fun onLocationChange(latitude:Double,longitude:Double)
}