package com.cmexpertise.mealkey.Utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.Transformation
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.google.android.material.snackbar.Snackbar
import org.aviran.cookiebar2.CookieBar
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


fun Int.toPx(context: Context) = (this * context.resources.displayMetrics.densityDpi) / DisplayMetrics.DENSITY_DEFAULT

fun Context.getColorRes(@ColorRes colorId: Int) = ContextCompat.getColor(this, colorId)

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

class Utils {

    companion object {

        private const val SECOND_MILLIS = 1000
        private const val MINUTE_MILLIS = 60 * SECOND_MILLIS
        private const val HOUR_MILLIS = 60 * MINUTE_MILLIS
        private const val DAY_MILLIS = 24 * HOUR_MILLIS

        val connectivityManager:ConnectivityManager = ApplicationClass.mInstance!!.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")

        public lateinit var currentFragment: Fragment


        fun getTime(timeStamp:Long):String{

            var time=timeStamp

            if (time < 1000000000000L) {
                time *= 1000;
            }

            val now = currentDate().time
            if (time > now || time <= 0) {
                return "in the future"
            }

            val diff = now - time
            return when {
                diff < MINUTE_MILLIS -> "Just now"
                diff < 2 * MINUTE_MILLIS -> "a minute ago"
                diff < 60 * MINUTE_MILLIS -> "${diff / MINUTE_MILLIS} minutes ago"
                diff < 2 * HOUR_MILLIS -> "an hour ago"
                diff < 24 * HOUR_MILLIS -> "${diff / HOUR_MILLIS} hours ago"
                diff < 48 * HOUR_MILLIS -> "yesterday"
                else -> "${diff / DAY_MILLIS} days ago"
            }
        }

        private fun currentDate(): Date {
            val calendar = Calendar.getInstance()
            return calendar.time
        }

        fun expand(v: View) {
            v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            val targetHeight = v.measuredHeight
            // Older versions of android (pre API 21) cancel animations for views with a height of 0.
            v.layoutParams.height = 1
            v.visibility = View.VISIBLE
            val a: Animation = object : Animation() {
                override fun applyTransformation(
                    interpolatedTime: Float,
                    t: Transformation?
                ) {
                    v.layoutParams.height =
                        if (interpolatedTime == 1f) LinearLayout.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }

                override fun willChangeBounds(): Boolean {
                    return true
                }
            }
            // 1dp/ms
            a.setDuration((targetHeight / v.context.resources.displayMetrics.density).toLong())
            v.startAnimation(a)
        }


        fun collapse(v: View) {
            val initialHeight = v.measuredHeight
            val a: Animation = object : Animation() {
                override fun applyTransformation(
                    interpolatedTime: Float,
                    t: Transformation?
                ) {
                    if (interpolatedTime == 1f) {
                        v.visibility = View.GONE
                    } else {
                        v.layoutParams.height =
                            initialHeight - (initialHeight * interpolatedTime).toInt()
                        v.requestLayout()
                    }
                }

                override fun willChangeBounds(): Boolean {
                    return true
                }
            }
            // 1dp/ms
            a.setDuration((initialHeight / v.context.resources.displayMetrics.density).toLong())

            v.startAnimation(a)
        }


        fun getCurrentDate():String{
            val sdf = SimpleDateFormat("MMM, dd EEE")
            val currentDate = sdf.format(Date())
            return currentDate
        }
        fun getCurrentDate2():String{
            val sdf = SimpleDateFormat("dd MMM YYYY")
            val currentDate = sdf.format(Date())
            return currentDate
        }

        fun getCurrentDateTime():String{

            val currentDate = sdf.format(Date())
            return currentDate
        }
        fun getCurrentDateTime2():String{

            val currentDate = sdf.format(Date())
            return currentDate
        }
        fun getDateForamtDate(strDate:String):String{
            val inputPattern = "dd-MM-yyyy"
            val outputPattern = "dd"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            var date: Date? = null
            var str: String? = null

            try {
                date = inputFormat.parse(strDate)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return str!!
        }
        fun getDateForamtMonthYear(strDate:String):String{
            val inputPattern = "dd-MM-yyyy"
            val outputPattern = "MMM yy"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            var date: Date? = null
            var str: String? = null

            try {
                date = inputFormat.parse(strDate)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return str!!
        }
        fun getDateForamtMonth(strDate:String):String{
            val inputPattern = "dd-MM-yyyy"
            val outputPattern = "MMM"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            var date: Date? = null
            var str: String? = null

            try {
                date = inputFormat.parse(strDate)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return str!!
        }

        fun getDateOrder(strDate:String):String{
            val inputPattern = "yyyy-MM-dd"
            val outputPattern = "dd MMM yyyy"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            var date: Date? = null
            var str: String? = null

            try {
                date = inputFormat.parse(strDate)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return str!!
        }

        fun getVotingdate(strDate:String):String{
            val inputPattern = "yyyy-MM-dd hh:mm:ss"
            val outputPattern = "hh:mm a"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            var date: Date? = null
            var str: String? = null

            try {
                date = inputFormat.parse(strDate)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return str!!
        }

        fun compareDate(startTime:String,endtime:String):Boolean{
            try {
                Log.d("StartTime-endtime","$startTime - $endtime")


                val formatter = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
                val date1 = formatter.parse(startTime)
                val date2 = formatter.parse(endtime)
                if (date1.after(date2)) {
                    return true
                }else
                {
                    return false
                }
            } catch (e1: ParseException) {
                e1.printStackTrace()
            }
            return false
        }
        fun colorMyText(inputText:String,startIndex:Int,endIndex:Int,textColor:Int): Spannable {
            val outPutColoredText: Spannable = SpannableString(inputText)
            outPutColoredText.setSpan(
                ForegroundColorSpan(textColor), startIndex, endIndex,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return outPutColoredText
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun setStatusBarColor(activity: Activity, color: Int) {
            val window = activity.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.decorView.systemUiVisibility=View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor = color
        }
        fun setLightStatusBar(view: View) {
            var flags = view.systemUiVisibility
            flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            view.systemUiVisibility = flags
        }



        fun isOnline(context: Activity, message: Boolean): Boolean {
            var result = false

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                val networkCapabilities = connectivityManager.activeNetwork ?: null
                val actN = connectivityManager.getNetworkCapabilities(networkCapabilities) ?: null
                actN?.let {
                    result = when{
                        actN!!.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                        actN!!.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                        actN!!.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                        else -> false
                    }
                }

            }else{
                connectivityManager.run {
                    connectivityManager.activeNetworkInfo?.run {
                        result = when(type){
                            ConnectivityManager.TYPE_WIFI -> true
                            ConnectivityManager.TYPE_MOBILE -> true
                            ConnectivityManager.TYPE_ETHERNET -> true
                            else -> false
                        }
                    }
                }
            }

            if(!result && message){
                showToast(context,context.resources.getString(R.string.check_connection))
            }

            return result
        }
        fun showToast(activity: Activity,msg:String){

            CookieBar.build(activity)
                .setMessage(msg)
                .setCookiePosition(CookieBar.TOP)
                .setDuration(2000)
                .setBackgroundColor(R.color.colorPrimary)
                .show()
//            Toast.makeText(activity,msg,Toast.LENGTH_SHORT).show()
        }


        fun getDeviceResolution(activity: Activity?): DisplayMetrics {
            val displaymatrics = DisplayMetrics()
            activity?.windowManager?.defaultDisplay?.getMetrics(displaymatrics)
            return displaymatrics
        }

        fun isValidEmail(inputmail: CharSequence): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(inputmail).matches()
        }

        fun Context.hideKeyboard(view: View) {
            val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun snackBar(view: View, msg: String, isSnackBar: Boolean, context: Context?) {
            try {
                if (isSnackBar) {
                    val snackBar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                    snackBar.view.setBackgroundColor(Color.parseColor("#B10101"))
                    snackBar.show()
                } else {

                    showToast((context as Activity?)!!, "$msg")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun checkPermissionForLocation(activity: Activity): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                if (activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
                    true
                } else {
                    // Show the permission request
                    ActivityCompat.requestPermissions(activity, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                            15)
                    false
                }
            } else {
                true
            }
        }

        fun checkPermitionGallaryCamera(context: Activity?) {
            context?.let {
                requestPermissions(
                        it, arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ), 1
                )
            }

        }

        fun milliSecondsToTimer(milliseconds: Long): String? {
            var finalTimerString = ""
            var secondsString = ""

            // Convert total duration into time
            val hours = (milliseconds / (1000 * 60 * 60)).toInt()
            val minutes = (milliseconds % (1000 * 60 * 60)).toInt() / (1000 * 60)
            val seconds = (milliseconds % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()
            // Add hours if there
            if (hours > 0) {
                finalTimerString = "$hours:"
            }

            // Prepending 0 to seconds if it is one digit
            secondsString = if (seconds < 10) {
                "0$seconds"
            } else {
                "" + seconds
            }
            finalTimerString = "$finalTimerString$minutes:$secondsString"

            // return timer string
            return finalTimerString
        }

        /**
         * Function to get Progress percentage
         * @param currentDuration
         * @param totalDuration
         */
        fun getProgressPercentage(currentDuration: Long, totalDuration: Long): Int {
            var percentage = 0.toDouble()
            val currentSeconds: Long = (currentDuration / 1000).toLong()
            val totalSeconds: Long = (totalDuration / 1000).toLong()

            // calculating percentage
            percentage = currentSeconds.toDouble() / totalSeconds * 100

            // return percentage
            return percentage.toInt()
        }

        fun progressToTimer(progress: Int, totalDuration: Int): Int {
            var totalDuration = totalDuration
            var currentDuration = 0
            totalDuration = (totalDuration / 1000)
            currentDuration = (progress.toDouble() / 100 * totalDuration).toInt()

            // return current duration in milliseconds
            return currentDuration * 1000
        }

        fun changeDateFormate(timer: String, toFormat: String, fromdate: String): String {
            val datesource = SimpleDateFormat(toFormat)
            val date = datesource.parse(timer)
            val datedestination = SimpleDateFormat(fromdate)
            return datedestination.format(date)
        }


        fun addNextFragment(
                mActivity: Activity,
                targetedFragment: androidx.fragment.app.Fragment,
                shooterFragment: androidx.fragment.app.Fragment,
                isDownToUp: Boolean
        ) {
            val transaction=ApplicationClass.getmInstance()!!.getactivity()!!.supportFragmentManager.beginTransaction()


            if (isDownToUp) {
                transaction.setCustomAnimations(
                    R.animator.slide_fragment_vertical_right_in,
                    R.animator.slide_fragment_vertical_left_out,
                    R.animator.slide_fragment_vertical_left_in,
                    R.animator.slide_fragment_vertical_right_out
                )

            } else {
                transaction.setCustomAnimations(
                        R.animator.slide_fragment_horizontal_right_in,
                        R.animator.slide_fragment_horizontal_left_out,
                        R.animator.slide_fragment_horizontal_left_in,
                        R.animator.slide_fragment_horizontal_right_out
                )

            }


            transaction.add(
                    R.id.container,
                    targetedFragment,
                    targetedFragment.javaClass.getSimpleName()
            );
            currentFragment = targetedFragment;
            transaction.hide(shooterFragment)
            transaction.addToBackStack(targetedFragment.javaClass.simpleName)
            transaction.commit()
        }
        fun hideKeyboard(mContext: Context) {
            val inputManager =
                    mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

            val focus = (mContext as Activity).currentFocus

            if (focus != null) {

                inputManager.hideSoftInputFromWindow(
                        focus.windowToken,
                        InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
        }

        fun hideKeyboardWithDialog(mContext: Context) {
            val imm: InputMethodManager =
                mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        }


        fun hideSoftKeyboardWhenNeeded(activity: Activity) {
            val inputMethodManager =
                    activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            if (inputMethodManager.isActive) {
                if (activity.currentFocus != null) {
                    inputMethodManager.hideSoftInputFromWindow(
                            activity.currentFocus!!.windowToken,
                            InputMethodManager.HIDE_NOT_ALWAYS
                    )
                }
            }
        }
        fun snackbar(view: View, msg: String, isSnakbar: Boolean, mContext: Context) {

            try {
                if (isSnakbar) {
                    val snack = Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                    snack.view.setBackgroundColor(Color.parseColor("#F0627E"))
                    val viewNew = snack.view
                    val tv = viewNew.findViewById<View>(R.id.snackbar_text) as TextView
                    tv.gravity = Gravity.CENTER_HORIZONTAL
                    snack.show()
                } else {
                    Toast.makeText(mContext, "" + msg, Toast.LENGTH_LONG).show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


        }



    }







}