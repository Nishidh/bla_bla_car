package com.cmexpertise.mealkey.mvvm.profile

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cmexpertise.mealkey.Api.UsesCaseResult
import com.cmexpertise.mealkey.Repository.YourDetailRepository
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.model.your_detail.YourDetailResponse
import kotlinx.coroutines.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import kotlin.coroutines.CoroutineContext

open class YourDetailViewModel(private val profileRepository: YourDetailRepository): ViewModel(), CoroutineScope {

    val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.Main + job

    val showloding = MutableLiveData<Boolean>()
    val responseData = MutableLiveData<YourDetailResponse>()
    val showerror = MutableLiveData<String>()

    private val context : Context = ApplicationClass.mInstance!!.applicationContext

    fun doProfileUpdateWithImage(userId: RequestBody,firstName: RequestBody, lastname:RequestBody,gender:RequestBody,birthYear:RequestBody,mobile:RequestBody,countryCode:RequestBody,email:RequestBody,mini_bio:RequestBody,deviceId:RequestBody,deviceToken:RequestBody,deviceType:RequestBody,image:MultipartBody.Part?){

        launch {
            val result = withContext(Dispatchers.IO) {
                profileRepository.profileWithImage(userId, firstName, lastname, gender, birthYear, mobile, countryCode, email, mini_bio, deviceId, deviceToken, deviceType, image)
            }
            showloding.value = false
            when(result){
                is UsesCaseResult.Success -> {
                    withContext(Dispatchers.Main){
                        responseData.value= result.data
                    }
                }
                is UsesCaseResult.Failed -> {
                    showerror.value = result.exception.message

                }
            }
        }
    }





    fun doProfileUpdateWithoutImage(userId: RequestBody,firstName: RequestBody, lastname:RequestBody,gender:RequestBody,birthYear:RequestBody,mobile:RequestBody,countryCode:RequestBody,email:RequestBody,mini_bio:RequestBody,deviceId:RequestBody,deviceToken:RequestBody,deviceType:RequestBody){
        launch {
            val result = withContext(Dispatchers.IO) {
                profileRepository.profileWithoutImage(userId, firstName, lastname, gender, birthYear, mobile, countryCode, email, mini_bio, deviceId, deviceToken, deviceType)
            }
            showloding.value = false
            when(result){
                is UsesCaseResult.Success -> {
                    withContext(Dispatchers.Main){
                        responseData.value= result.data
                    }
                }
                is UsesCaseResult.Failed -> {
                    showerror.value = result.exception.message
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

}