package com.example.ridecarapp.mvvm.login_new

import android.content.Context
import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cmexpertise.mealkey.Api.UsesCaseResult
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.Repository.BrandListRepository
import com.example.ridecarapp.Repository.CheckOtpRepository
import com.example.ridecarapp.Repository.LoginRepository
import com.example.ridecarapp.Repository.VehicleModelListRepository
import com.example.ridecarapp.model.login.LoginResponse
import com.example.ridecarapp.model.otp.CheckOtpResponse
import com.example.ridecarapp.model.vehiclebrand.BrandResponse
import com.example.ridecarapp.model.vehiclemodel.VehicleModelResposne
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


open class VehicleModelViewModel(private val vehicleModelListRepository: VehicleModelListRepository): ViewModel(), CoroutineScope {

    val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.Main + job

    val showloding = MutableLiveData<Boolean>()
    val responseData = MutableLiveData<VehicleModelResposne>()
    val showerror = MutableLiveData<String>()

    private val context : Context = ApplicationClass.mInstance!!.applicationContext

    fun getModelList(id:String?){

        launch {
            val result = withContext(Dispatchers.IO) {
                vehicleModelListRepository.getModelList(id!!)
            }

            showloding.value = false
            when(result){
                is UsesCaseResult.Success -> {
                    withContext(Dispatchers.Main){
                        responseData.value= result.data
                    }
                }
                is UsesCaseResult.Failed -> {
                    showerror.value = result.exception.message
                }
            }
        }
    }



    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }


}