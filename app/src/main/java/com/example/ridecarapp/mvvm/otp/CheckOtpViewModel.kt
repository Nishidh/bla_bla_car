package com.example.ridecarapp.mvvm.login_new

import android.content.Context
import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cmexpertise.mealkey.Api.UsesCaseResult
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.ApplicationClass
import com.example.ridecarapp.R
import com.example.ridecarapp.Repository.CheckOtpRepository
import com.example.ridecarapp.Repository.LoginRepository
import com.example.ridecarapp.model.login.LoginResponse
import com.example.ridecarapp.model.otp.CheckOtpResponse
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


open class CheckOtpViewModel(private val checkOtpRepository: CheckOtpRepository): ViewModel(), CoroutineScope {

    val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.Main + job

    val showloding = MutableLiveData<Boolean>()
    val responseData = MutableLiveData<CheckOtpResponse>()
    val loginData = MutableLiveData<LoginResponse>()
    val showerror = MutableLiveData<String>()

    private val context : Context = ApplicationClass.mInstance!!.applicationContext

    fun checkOtp(mobile: String?, countryCode: String?,otp:String?){

        launch {
            val result = withContext(Dispatchers.IO) {
                checkOtpRepository.checkOtp(mobile!!, countryCode!!,otp!!)
            }

            showloding.value = false
            when(result){
                is UsesCaseResult.Success -> {
                    withContext(Dispatchers.Main){
                        responseData.value= result.data
                    }
                }
                is UsesCaseResult.Failed -> {
                    showerror.value = result.exception.message
                }
            }
        }
    }

    fun doGetOtp(mobile: String?, countryCode: String?){

        launch {
            val result = withContext(Dispatchers.IO) {
                checkOtpRepository.getOtp(mobile!!, countryCode!!)
            }

            showloding.value = false
            when(result){
                is UsesCaseResult.Success -> {
                    withContext(Dispatchers.Main){
                        loginData.value= result.data
                    }
                }
                is UsesCaseResult.Failed -> {
                    showerror.value = result.exception.message
                }
            }
        }
    }


    fun isValid(mobile:String):String{
        return if(TextUtils.isEmpty(mobile)){
            context.getString(R.string.str_enter_mobile_number)
        }else{
            ""
        }
    }



    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }


}