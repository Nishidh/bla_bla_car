package com.kinokeEvent.kinoke.mvvm.otherAlbums

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.RowFindRideBinding
import com.example.ridecarapp.fragments.CreateRideAlterFragment


class CreateRideAdapter(private val fragment: CreateRideAlterFragment, private val context: Context, private val items: ArrayList<TestModel>) : RecyclerView.Adapter<CreateRideAdapter.ViewHolder>() {

    private var binding: RowFindRideBinding? = null
    private var lastPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.row_find_ride, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position],position)

    inner class ViewHolder(val binding: RowFindRideBinding?) : RecyclerView.ViewHolder(binding!!.root) {
        fun bind(item: TestModel,pos:Int) {
            binding!!.executePendingBindings()

            itemView.setOnClickListener {
                fragment.onItemClick()
            }

            }
    }

}