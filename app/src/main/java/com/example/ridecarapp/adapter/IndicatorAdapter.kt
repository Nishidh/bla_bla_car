package com.kinokeEvent.kinoke.mvvm.otherAlbums

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil


import androidx.recyclerview.widget.RecyclerView
import com.cmexpertise.mealkey.models.IntroModel
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.RowIndicatorBinding

class IndicatorAdapter(private val context: Context, private val items: ArrayList<IntroModel>) : RecyclerView.Adapter<IndicatorAdapter.ViewHolder>() {

    private var binding: RowIndicatorBinding? = null
    private var lastpostion=0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_indicator,
            parent,
            false
        )
        return ViewHolder(binding!!)
    }



    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position],position)

    fun updateLastPosition(lastpos:Int){
        lastpostion=lastpos
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: RowIndicatorBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: IntroModel, position: Int) {
            if(lastpostion == position){
                binding.ivImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.drawable_indicator_select))
            }else{
                binding.ivImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.drawable_indicator_unselect))
            }
            binding.executePendingBindings()
        }
    }
}