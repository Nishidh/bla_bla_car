package com.kinokeEvent.kinoke.mvvm.otherAlbums

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.RowEditPriceBinding
import com.example.ridecarapp.databinding.RowFindRideBinding
import com.example.ridecarapp.databinding.RowInboxBinding
import com.example.ridecarapp.fragments.CreateRideAlterFragment


class EditPriceAdapter(private val context: Context, private val items: ArrayList<TestModel>) : RecyclerView.Adapter<EditPriceAdapter.ViewHolder>() {

    private var binding: RowEditPriceBinding? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.row_edit_price, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position],position)

    inner class ViewHolder(val binding: RowEditPriceBinding?) : RecyclerView.ViewHolder(binding!!.root) {
        fun bind(item: TestModel,pos:Int) {
            binding!!.executePendingBindings()

            }
    }

}