package com.kinokeEvent.kinoke.mvvm.otherAlbums

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cmexpertise.mealkey.Utils.Utils
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.RowLeftBinding
import com.example.ridecarapp.databinding.RowRightBinding
import com.example.ridecarapp.model.ChatMessage


class ChatAdapter(private val context: Context, private val items: ArrayList<ChatMessage>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var bindingRightMsg: RowRightBinding
    private lateinit var bindingLeftMsg: RowLeftBinding
    private var RIGHT_MSG = 0
    private var LEFT_MSG = 1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == LEFT_MSG) {
            bindingLeftMsg = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.row_left,
                parent,
                false
            )
            return ViewHolderLeft(bindingLeftMsg)
        }
        bindingRightMsg = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_right,
            parent,
            false
        )
        return ViewHolderRight(bindingRightMsg)
    }

    override fun getItemViewType(position: Int): Int {
        if (items.get(position).isSender) return RIGHT_MSG else return LEFT_MSG

    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder.itemViewType) {
            LEFT_MSG -> {
                (holder as ViewHolderLeft).bind(items.get(position), position)
            }
            RIGHT_MSG -> {
                (holder as ViewHolderRight).bind(items.get(position), position)
            }
        }
    }

    inner class ViewHolderLeft(val binding: RowLeftBinding?) :
        RecyclerView.ViewHolder(binding!!.root) {
        fun bind(item: ChatMessage, pos: Int) {
            binding!!.executePendingBindings()
            binding.txtMsg.text=item.msg
            binding.tvTime.text=Utils.getTime(item.timeStemp.toLong())
        }
    }


    inner class ViewHolderRight(val binding: RowRightBinding?) :
        RecyclerView.ViewHolder(binding!!.root) {
        fun bind(item: ChatMessage, pos: Int) {
            binding!!.executePendingBindings()
            binding.txtMsg.text=item.msg
            binding.tvTime.text=Utils.getTime(item.timeStemp.toLong())
        }
    }

}