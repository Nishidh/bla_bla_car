package com.cmexpertise.mealkey.adapater

import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.cmexpertise.mealkey.models.IntroModel
import com.example.ridecarapp.R

class IntroScreenAdapter(var context: Context, private var list: List<IntroModel>) : PagerAdapter() {
    private val inflater: LayoutInflater
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


    override fun getCount(): Int {
        return list.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout: View = inflater.inflate(R.layout.row_intro, view, false)!!
        val tvTitle = imageLayout.findViewById<View>(R.id.tvTitle) as TextView
        val ivImge = imageLayout.findViewById<View>(R.id.img) as ImageView
        tvTitle.text=list[position].name
        ivImge.setImageDrawable(ContextCompat.getDrawable(context,list[position].imageId))
        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {
    }

    override fun saveState(): Parcelable? {
        return null
    }

    init {
        inflater = LayoutInflater.from(context)
    }
}