package com.example.ridecarapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.ridecarapp.fragments.*

class Pager(fm: FragmentManager?, var tabCount: Int) :
    FragmentStatePagerAdapter(fm!!) {

    private val tabTitles = arrayOf("Up-going", "Complete", "Archived")

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                UpgoingRideFragment()
            }
            1 -> {
                RideCompleteFragment()
            }
            2 -> {
                RideArchieveFragment()
            }
            else -> {
                RideDetailFragment()
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }

    private fun openFragment(){
    }

    override fun getCount(): Int {
        return tabCount
    }

}