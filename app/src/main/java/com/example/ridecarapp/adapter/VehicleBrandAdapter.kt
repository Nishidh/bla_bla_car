package com.kinokeEvent.kinoke.mvvm.otherAlbums

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.RowVehicleBrandBinding
import com.example.ridecarapp.fragments.VehicleBrandFragment
import com.example.ridecarapp.model.vehiclebrand.VehicleData


class VehicleBrandAdapter(
    private val fragment: VehicleBrandFragment,
    private val context: Context,
    private var items: ArrayList<VehicleData>
) : RecyclerView.Adapter<VehicleBrandAdapter.ViewHolder>() {

    private var binding: RowVehicleBrandBinding? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_vehicle_brand,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position], position)

    fun setList(list: List<VehicleData>)
    {
        items= list as ArrayList<VehicleData>
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: RowVehicleBrandBinding?) :
        RecyclerView.ViewHolder(binding!!.root) {
        fun bind(item: VehicleData, pos: Int) {
            binding!!.executePendingBindings()

            binding!!.tvName.text=item.brandName

            itemView.setOnClickListener {
                fragment.setOnItemClick(item)
            }
        }
    }

}