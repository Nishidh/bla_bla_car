package com.kinokeEvent.kinoke.mvvm.otherAlbums

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.RowVehicleBrandBinding
import com.example.ridecarapp.fragments.VehicleModelFragment
import com.example.ridecarapp.model.vehiclemodel.VehicleModelData


class VehicleModelAdapter(
    private val fragment: VehicleModelFragment,
    private val context: Context,
    private var items: ArrayList<VehicleModelData>
) : RecyclerView.Adapter<VehicleModelAdapter.ViewHolder>() {

    private var binding: RowVehicleBrandBinding? = null
    private var lastPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_vehicle_brand,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position], position)

    fun setList(list: List<VehicleModelData>) {
        items= list as ArrayList<VehicleModelData>
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: RowVehicleBrandBinding?) :
        RecyclerView.ViewHolder(binding!!.root) {
        fun bind(item: VehicleModelData, pos: Int) {
            binding!!.executePendingBindings()

            binding.tvName.text=item.models

            itemView.setOnClickListener {
                fragment.setOnItemClick(item)
            }
        }
    }

}