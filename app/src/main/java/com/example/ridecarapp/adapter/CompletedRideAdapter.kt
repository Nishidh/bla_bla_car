package com.kinokeEvent.kinoke.mvvm.otherAlbums

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cmexpertise.mealkey.Utils.Utils
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.RowCompleteBinding
import com.example.ridecarapp.databinding.RowUpgoingBinding
import com.example.ridecarapp.fragments.RideCompleteFragment
import com.example.ridecarapp.fragments.RideDetailFragment


class CompletedRideAdapter(private val fragment: RideCompleteFragment, private val context: Context, private val items: ArrayList<TestModel>) : RecyclerView.Adapter<CompletedRideAdapter.ViewHolder>() {

    private var binding: RowCompleteBinding? = null
    private var lastPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.row_complete, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position],position)

    inner class ViewHolder(val binding: RowCompleteBinding?) : RecyclerView.ViewHolder(binding!!.root) {
        fun bind(item: TestModel,pos:Int) {
            binding!!.executePendingBindings()

            binding.llLeaveRating.setOnClickListener {
                fragment.OnItemClick()
            }

            binding.rlProfile.setOnClickListener {
                fragment.onItemProfileClick()
            }

        }
    }

}