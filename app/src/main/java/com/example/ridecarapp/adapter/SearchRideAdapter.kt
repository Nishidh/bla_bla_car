package com.kinokeEvent.kinoke.mvvm.otherAlbums

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.SpannableStringBuilder
import android.text.style.ImageSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.RowSearchBinding
import com.example.ridecarapp.fragments.FindRideFragment


class SearchRideAdapter(private val fragment: FindRideFragment, private val context: Context, private val items: ArrayList<TestModel>) : RecyclerView.Adapter<SearchRideAdapter.ViewHolder>() {

    private var binding: RowSearchBinding? = null
    private var lastPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.row_search, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position],position)

    inner class ViewHolder(val binding: RowSearchBinding?) : RecyclerView.ViewHolder(binding!!.root) {
        @SuppressLint("NewApi")
        fun bind(item: TestModel, pos:Int) {
            binding!!.executePendingBindings()

            val myIcon: Drawable = context.getResources().getDrawable(R.drawable.ic_rightarrow,null)
            binding.textview?.getLineHeight()?.let { myIcon.setBounds(0, 0, it, binding.textview?.getLineHeight()!!) };
            val builder = SpannableStringBuilder()
            builder.append("Express Highway")
                .append(" ", ImageSpan(myIcon), 0)
                .append("Toll Plaza - Ahemdabad Vadodara Express Way [NE-1]")

            binding.textview?.text = builder

        }
    }

}