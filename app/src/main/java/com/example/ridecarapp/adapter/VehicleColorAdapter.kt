package com.kinokeEvent.kinoke.mvvm.otherAlbums

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.R
import com.example.ridecarapp.databinding.RowVehicleColorBinding
import com.example.ridecarapp.fragments.VehicleColorFragment

class VehicleColorAdapter(
    private val fragment: VehicleColorFragment,
    private val context: Context,
    private val items: ArrayList<TestModel>
) : RecyclerView.Adapter<VehicleColorAdapter.ViewHolder>() {

    private var binding: RowVehicleColorBinding? = null
    private var lastPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_vehicle_color,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position], position)

    inner class ViewHolder(val binding: RowVehicleColorBinding?) :
        RecyclerView.ViewHolder(binding!!.root) {
        fun bind(item: TestModel, pos: Int) {
            binding!!.executePendingBindings()
            itemView.setOnClickListener {
                fragment.setOnItemClick()
            }
        }
    }

}