package com.cmexpertise.mealkey.Repository


import com.cmexpertise.mealkey.Api.Apis
import com.cmexpertise.mealkey.Api.UsesCaseResult
import com.example.ridecarapp.model.your_detail.YourDetailResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody


interface YourDetailRepository {

    suspend fun profileWithImage(userId: RequestBody,firstName: RequestBody, lastname:RequestBody,gender:RequestBody,birthYear:RequestBody,mobile:RequestBody,countryCode:RequestBody,email:RequestBody,mini_bio:RequestBody,deviceId:RequestBody,deviceToken:RequestBody,deviceType:RequestBody,image:MultipartBody.Part?): UsesCaseResult<YourDetailResponse>

    suspend fun profileWithoutImage(userId: RequestBody,firstName: RequestBody, lastname:RequestBody,gender:RequestBody,birthYear:RequestBody,mobile:RequestBody,countryCode:RequestBody,email:RequestBody,mini_bio:RequestBody,deviceId:RequestBody,deviceToken:RequestBody,deviceType:RequestBody): UsesCaseResult<YourDetailResponse>

}

class YourDetailRepositoryImpl(private val apis: Apis): YourDetailRepository {
    override suspend fun profileWithImage(userId: RequestBody,firstName: RequestBody, lastname:RequestBody,gender:RequestBody,birthYear:RequestBody,mobile:RequestBody,countryCode:RequestBody,email:RequestBody,mini_bio:RequestBody,deviceId:RequestBody,deviceToken:RequestBody,deviceType:RequestBody,image:MultipartBody.Part?): UsesCaseResult<YourDetailResponse> {
        val result = apis.doUpdateUserDetailWithImage(userId,firstName,lastname,gender,birthYear,mobile,countryCode,email,mini_bio,deviceId,deviceToken,deviceType,image)!!.await()
        return try {
            UsesCaseResult.Success(result)
        }catch (e: Exception){
            UsesCaseResult.Failed(e)
        }
    }

    override suspend fun profileWithoutImage(userId: RequestBody,firstName: RequestBody, lastname:RequestBody,gender:RequestBody,birthYear:RequestBody,mobile:RequestBody,countryCode:RequestBody,email:RequestBody,mini_bio:RequestBody,deviceId:RequestBody,deviceToken:RequestBody,deviceType:RequestBody): UsesCaseResult<YourDetailResponse> {
        val result = apis.doUpdateUserDetailWithoutImage(userId,firstName,lastname,gender,birthYear,mobile,countryCode,email,mini_bio,deviceId,deviceToken,deviceType)!!.await()
        return try {
            UsesCaseResult.Success(result)
        }catch (e: Exception){
            UsesCaseResult.Failed(e)
        }
    }
}