package com.example.ridecarapp.Repository

import com.cmexpertise.mealkey.Api.Apis
import com.cmexpertise.mealkey.Api.UsesCaseResult
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.model.login.LoginResponse
import com.example.ridecarapp.model.otp.CheckOtpResponse


interface CheckOtpRepository {
    suspend fun checkOtp(mobile: String, countryCode:String,otp:String): UsesCaseResult<CheckOtpResponse>
    suspend fun getOtp(mobile: String, countryCode:String): UsesCaseResult<LoginResponse>
}

class CheckOtpRepositoryImpl(private val apis: Apis): CheckOtpRepository {
    override suspend fun checkOtp(mobile: String, countryCode: String,otp:String): UsesCaseResult<CheckOtpResponse> {
        val result = apis.doCheckOtp(mobile,countryCode,otp).await()
        return try {
            UsesCaseResult.Success(result)
        }catch (e: Exception){
            UsesCaseResult.Failed(e)
        }
    }

    override suspend fun getOtp(mobile: String, countryCode: String): UsesCaseResult<LoginResponse> {
        val result = apis.doLogin(mobile,countryCode).await()
        return try {
            UsesCaseResult.Success(result)
        }catch (e: Exception){
            UsesCaseResult.Failed(e)
        }
    }
}
