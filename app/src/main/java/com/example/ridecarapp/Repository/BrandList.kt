package com.example.ridecarapp.Repository

import com.cmexpertise.mealkey.Api.Apis
import com.cmexpertise.mealkey.Api.UsesCaseResult
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.model.login.LoginResponse
import com.example.ridecarapp.model.otp.CheckOtpResponse
import com.example.ridecarapp.model.vehiclebrand.BrandResponse


interface BrandListRepository {
    suspend fun getBrandList(): UsesCaseResult<BrandResponse>
}

class BrandListRepositoryImpl(private val apis: Apis): BrandListRepository {
    override suspend fun getBrandList(): UsesCaseResult<BrandResponse> {
        val result = apis.getBrandList().await()
        return try {
            UsesCaseResult.Success(result)
        }catch (e: Exception){
            UsesCaseResult.Failed(e)
        }
    }


}
