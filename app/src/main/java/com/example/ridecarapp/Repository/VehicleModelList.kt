package com.example.ridecarapp.Repository

import com.cmexpertise.mealkey.Api.Apis
import com.cmexpertise.mealkey.Api.UsesCaseResult
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.model.login.LoginResponse
import com.example.ridecarapp.model.otp.CheckOtpResponse
import com.example.ridecarapp.model.vehiclebrand.BrandResponse
import com.example.ridecarapp.model.vehiclemodel.VehicleModelResposne


interface VehicleModelListRepository {
    suspend fun getModelList(id:String): UsesCaseResult<VehicleModelResposne>
}

class VehicleModelListRepositoryImpl(private val apis: Apis): VehicleModelListRepository {
    override suspend fun getModelList(id:String): UsesCaseResult<VehicleModelResposne> {
        val result = apis.getModelList(id).await()
        return try {
            UsesCaseResult.Success(result)
        }catch (e: Exception){
            UsesCaseResult.Failed(e)
        }
    }


}
