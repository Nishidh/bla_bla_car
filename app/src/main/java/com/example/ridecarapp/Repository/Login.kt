package com.example.ridecarapp.Repository

import com.cmexpertise.mealkey.Api.Apis
import com.cmexpertise.mealkey.Api.UsesCaseResult
import com.cmexpertise.mealkey.models.TestModel
import com.example.ridecarapp.model.login.LoginResponse


interface LoginRepository {
    suspend fun getOtp(mobile: String, countryCode:String): UsesCaseResult<LoginResponse>
}

class LoginNewRepositoryImpl(private val apis: Apis): LoginRepository {
    override suspend fun getOtp(mobile: String, countryCode: String): UsesCaseResult<LoginResponse> {
        val result = apis.doLogin(mobile,countryCode).await()
        return try {
            UsesCaseResult.Success(result)
        }catch (e: Exception){
            UsesCaseResult.Failed(e)
        }
    }
}
