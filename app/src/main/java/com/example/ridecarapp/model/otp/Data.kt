package com.example.ridecarapp.model.otp


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("birth_year")
    val birthYear: String,
    @SerializedName("country_code")
    val countryCode: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("is_verified")
    val isVerified: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("mini_bio")
    val miniBio: String,
    @SerializedName("mobile")
    val mobile: String,
    @SerializedName("otp")
    val otp: String,
    @SerializedName("profile")
    val profile: String,
    @SerializedName("token")
    val token: String,
    @SerializedName("updated_at")
    val updatedAt: String
)