package com.example.ridecarapp.model.otp


import com.google.gson.annotations.SerializedName

data class CheckOtpResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
)