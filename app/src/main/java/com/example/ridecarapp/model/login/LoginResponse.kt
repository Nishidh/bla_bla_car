package com.example.ridecarapp.model.login


import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("message")
    val message: String,
    @SerializedName("otp")
    val otp: Int,
    @SerializedName("status")
    val status: Int,
    @SerializedName("user_id")
    val userId: String
)