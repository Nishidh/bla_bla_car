package com.example.ridecarapp.model.your_detail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class YourDetailResponse {
    @SerializedName("status")
    @Expose
   public var status = 0

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("data")
    @Expose
     var data: YourDetailResponseData? = null
}