package com.example.ridecarapp.model.your_detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YourDetailResponseData {

@SerializedName("id")
@Expose
public String id;
@SerializedName("first_name")
@Expose
public String firstName;
@SerializedName("last_name")
@Expose
public String lastName;
@SerializedName("gender")
@Expose
public String gender;
@SerializedName("birth_year")
@Expose
public String birthYear;
@SerializedName("profile")
@Expose
public String profile;
@SerializedName("country_code")
@Expose
public String countryCode;
@SerializedName("mobile")
@Expose
public String mobile;
@SerializedName("otp")
@Expose
public String otp;
@SerializedName("is_verified")
@Expose
public String isVerified;
@SerializedName("email")
@Expose
public String email;
@SerializedName("mini_bio")
@Expose
public String miniBio;
@SerializedName("token")
@Expose
public String token;
@SerializedName("created_at")
@Expose
public String createdAt;
@SerializedName("updated_at")
@Expose
public String updatedAt;

}