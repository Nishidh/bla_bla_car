package com.example.ridecarapp.model.vehiclemodel


import com.google.gson.annotations.SerializedName

data class VehicleModelData(
    @SerializedName("id")
    val id: String,
    @SerializedName("models")
    val models: String,
    @SerializedName("vehicle_models_brand_id")
    val vehicleModelsBrandId: String
)