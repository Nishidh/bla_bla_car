package com.example.ridecarapp.model.vehiclebrand


import com.google.gson.annotations.SerializedName

data class BrandResponse(
    @SerializedName("data")
    val `data`: List<VehicleData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
)