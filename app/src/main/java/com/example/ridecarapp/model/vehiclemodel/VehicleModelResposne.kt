package com.example.ridecarapp.model.vehiclemodel


import com.google.gson.annotations.SerializedName

data class VehicleModelResposne(
    @SerializedName("data")
    val `data`: List<VehicleModelData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
)