package com.example.ridecarapp.model.vehiclebrand


import com.google.gson.annotations.SerializedName

data class VehicleData(
    @SerializedName("brand_name")
    val brandName: String,
    @SerializedName("id")
    val id: String
)